import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-view-chart',
  templateUrl: './view-chart.component.html',
  styleUrls: ['./view-chart.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ViewChartComponent implements OnInit {

  typeChart: any;
  dataChart: any;
  optionsChart: any;

  typeChartV: any;
  dataChartV: any;
  optionsChartV: any;

  typeChartP: any;
  dataChartP: any;
  optionsChartP: any;

  amp = [];
  dt = [];
  power = [];
  volt = [];

  constructor(private matDialogRef: MatDialogRef<ViewChartComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    const stringData = JSON.stringify(data);
    const objData = JSON.parse(stringData);

    // console.log(data.data)

    const count = objData.data.length;
    for (let i = 0; i < count; i++) {
      this.amp.push(data.data[i].amp);
      this.dt.push(data.data[i].dt);
      this.volt.push(data.data[i].vol);
      this.power.push(data.data[i].vol * data.data[i].amp);
    }

    // console.log(this.amp)

  }

  ngOnInit() {
    this.writeAmp();
    this.writePower();
    this.writeVolt();
  }

  writeVolt() {
    this.typeChartV = 'line';
    this.dataChartV = {
      labels: this.dt,
      datasets: [
        {
          label: 'Volt',
          data: this.volt,
          backgroundColor : '#FCAEBB'
        }
      ]
    };
    this.optionsChartV = {
      responsive: true,
      maintainAspectRatio: false
    };
  }

  writeAmp() {
    this.typeChart = 'line';
    this.dataChart = {
      labels: this.dt,
      datasets: [
        {
          label: 'Amp',
          data: this.amp,
          backgroundColor : '#9DE7D7'
        }
      ]
    };
    this.optionsChart = {
      responsive: true,
      maintainAspectRatio: false
    };
  }

  writePower() {
    this.typeChartP = 'line';
    this.dataChartP = {
      labels: this.dt,
      datasets: [
        {
          label: 'Power',
          data: this.power,
          backgroundColor : '#F1B2DC'
        }
      ]
    };
    this.optionsChartP = {
      responsive: true,
      maintainAspectRatio: false,

    };
  }


}
