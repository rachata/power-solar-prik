import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';



export interface FormModel {
  name: string;
  place: string;
  lat: number;
  lng: number;
  idUser: number;
  idDevice: number;
}

@Component({
  selector: 'app-dialog-edit',
  templateUrl: './dialog-edit.component.html',
  styleUrls: ['./dialog-edit.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DialogEditComponent implements OnInit {

  formData: FormData = new FormData();
  formGroup: FormGroup;

  txtLat: number;
  txtLng: number;
  txtName: string;
  txtPlace: string;

  lat = 7.1545593;
  lng = 100.6223298;
  zoom = 15;
  markers: Array<{ lat: number, lng: number }> = [];

    constructor(private form: FormBuilder,
                private matDialogRef: MatDialogRef<DialogEditComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private http: HttpClient,
                private router: Router
              ) {
                this.txtName = this.data.Name;
                this.txtPlace = this.data.Place;
                this.txtLat = this.data.Lat;
                this.txtLng = this.data.Lng;

                this.formGroup = this.createForm({
                  name: data.Name,
                  place: data.Place,
                  lat: data.Lat,
                  lng: data.Lng,
                  idUser: data.Usr,
                  idDevice: data.Id
                });

                this.markers.push({ lat: data.Lat, lng: data.Lng});
                this.lat = data.Lat;
                this.lng = data.Lng;

               }

  ngOnInit() {
  }

  markMap(e) {
    // console.log(e);
    if (this.markers.length > 0) {
      this.markers.pop();
    }
    this.markers.push({ lat: e.coords.lat, lng: e.coords.lng });
    this.formGroup.value.lat = e.coords.lat;
    this.formGroup.value.lng = e.coords.lng;
    this.txtLat = e.coords.lat;
    this.txtLng = e.coords.lng;
  }

  private createForm(model: FormModel): FormGroup {
    return this.form.group(model);
  }

  postData() {
    const body = this.formGroup.value;
    // console.log(this.formGroup);
    this.http.put(environment.apiUrl + '/device', body).subscribe( data => {
      // console.log(data);
      const status = JSON.parse(JSON.stringify(data));
      if (status.idStatus === 200){
        alert('Update Success');
        this.matDialogRef.close();
        this.router.navigateByUrl('/devices', {skipLocationChange: true});
      } else {
        alert('Error');
      }
    });
  }

}
