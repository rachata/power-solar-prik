import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from '../auth.service';
import { sha256 } from 'js-sha256';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

export interface ModelLogin {
  username: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  formData: FormData = new FormData();
  formGroup: FormGroup = this.createForm({
    username: '',
    password: ''
  });


  constructor(private form: FormBuilder, private authService: AuthService, private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  private createForm(model: ModelLogin): FormGroup {
    return this.form.group(model);
  }

  logedIn() {
    const hash = sha256.hex(this.formGroup.value.password);
    this.http.post<ModelLogin>(environment.apiUrl + '/user/login', { username: this.formGroup.value.username,
                                                                    password: hash } ).subscribe( data => {

    const status = JSON.parse(JSON.stringify(data));

    if (status.idStatus === 200) {
      this.authService.sendToken(status.id);
      this.router.navigateByUrl('viewmaps');
    } else {
      alert('fail');
    }

    });
  }

}
