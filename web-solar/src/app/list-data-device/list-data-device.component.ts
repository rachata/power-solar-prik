import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { ViewChartComponent } from '../view-chart/view-chart.component';
import { ExcelServiceService } from '../excel-service.service';

export interface PeriodicElement {
  vol: number;
  idLog: number;
  amp: number;
  dt: string;
}

export interface ExcelModel {
  logId: number;
  amp: number;
  volt: number;
  deviceId: number;
  dateTime: string;
}

@Component({
  selector: 'app-list-data-device',
  templateUrl: './list-data-device.component.html',
  styleUrls: ['./list-data-device.component.css']
})
export class ListDataDeviceComponent implements OnInit {

  private Id: any;
  private Usr: any;
  private allData: any;


  displayedColumns: string[] = ['idLog', 'vol', 'amp', 'power', 'dt'];
  dataSource = new MatTableDataSource<PeriodicElement>();

  @ViewChild(MatSort, {static : false}) sort: MatSort;

  excel: Array<{
    logId: any;
    amp: any;
    volt: any;
    power: any;
    deviceId: any;
    dateTime: any;
  }> = [];

  constructor(private dialog: MatDialog, private http: HttpClient, private route: ActivatedRoute,
              private excelService: ExcelServiceService) {
    this.Id = this.route.snapshot.paramMap.get('id');
    this.Usr = this.route.snapshot.paramMap.get('usr');
  }

  ngOnInit() {
    this.feedData();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  feedData() {
    this.http.get(environment.apiUrl + '/device/log/' + this.Id + '/' + this.Usr).subscribe(response => {
      const data = JSON.parse(JSON.stringify(response));
      if (data.data != null) {
        this.dataSource.data = data.data as PeriodicElement[];
        this.allData = JSON.parse(JSON.stringify(data));
        const count = data.data.length;
        for (let i = 0; i < count; i++) {
          this.excel.push({ logId: data.data[i].idLog, amp: data.data[i].amp, volt: data.data[i].vol, deviceId: data.data[i].idDevice,
            power: data.data[i].vol * data.data[i].amp, dateTime: data.data[i].dt });
        }
      } else {
        this.allData = null;
      }
    });
  }

  goChart() {
    if (this.allData != null) {
      this.dialog.open(ViewChartComponent, {
        height: '550px',
        width: '600px',
        data: this.allData
      });
    } else {
      alert('No Data');
    }
  }

  exportAsXLSX() {
    if (this.allData != null) {
      this.excelService.exportAsExcelFile(this.excel, 'Solar');
    } else {
      alert('No Data');
    }

  }

}
