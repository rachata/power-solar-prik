import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDataDeviceComponent } from './list-data-device.component';

describe('ListDataDeviceComponent', () => {
  let component: ListDataDeviceComponent;
  let fixture: ComponentFixture<ListDataDeviceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListDataDeviceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDataDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
