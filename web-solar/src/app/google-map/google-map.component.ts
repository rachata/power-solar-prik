import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { DialogStatusComponent } from '../dialog-status/dialog-status.component';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})

export class GoogleMapComponent implements OnInit {
  lat = 7.1545593;
  lng = 100.6223298;
  zoom = 15;
  markers: Array<{ lati: number, long: number }> = [];

  constructor(private http: HttpClient, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.http.get(environment.apiUrl + '/device/1').subscribe(data => {
      const dataString = JSON.stringify(data);
      const dataObj = JSON.parse(dataString);

      const dataSize = dataObj.data.length;

      for (let i = 0; i < dataSize; i++) {
        this.markers.push({ lati: dataObj.data[i].lat, long: dataObj.data[i].lng });
      }
    });
  }

  // clickMaker(e) {
  //   this.http.get(environment.apiUrl + '/device/1').subscribe(data => {
  //     const dataString = JSON.stringify(data);
  //     const datObj = JSON.parse(dataString);
  //     console.log(datObj);
  //     const dataSize = datObj.data.length;

  //     for (let i = 0; i < dataSize; i++) {
  //       if (dataSize !== 0 && e.latitude === datObj.data[i].lat && e.longitude === datObj.data[i].lng ) {
  //         this.router.navigate(['/data/' + datObj.data[i].idUser + '/' + datObj.data[i].idDevice]);
  //       }
  //     }

  //   });
  // }

  viewStatus(e) {

    this.http.get(environment.apiUrl + '/device/1').subscribe(data => {
      const dataString = JSON.stringify(data);
      const datObj = JSON.parse(dataString);
      console.log(datObj);
      const dataSize = datObj.data.length;

      for (let i = 0; i < dataSize; i++) {
        if (dataSize !== 0 && e.latitude === datObj.data[i].lat && e.longitude === datObj.data[i].lng ) {
          // this.router.navigate(['/data/' + datObj.data[i].idUser + '/' + datObj.data[i].idDevice]);
          this.dialog.open(DialogStatusComponent, {width : '400px',
                                             height : '300px',
                                             data : {
                                                     Device: datObj.data[i].idDevice,
                                                     Usr: datObj.data[i].idUser,
                                                     Name : datObj.data[i].name,
                                                     Place : datObj.data[i].place
                                                    }
                                              } );
        }
      }

    });
  }

}
