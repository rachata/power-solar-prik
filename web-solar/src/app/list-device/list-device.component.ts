import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { DialogEditComponent } from '../dialog-edit/dialog-edit.component';

@Component({
  selector: 'app-list-device',
  templateUrl: './list-device.component.html',
  styleUrls: ['./list-device.component.css']
})
export class ListDeviceComponent implements OnInit {

  public objData: any;

  constructor(private http: HttpClient, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.http.get(environment.apiUrl + '/device/1').subscribe(data => {
      const realData = JSON.stringify(data);
      const dataObj = JSON.parse(realData);
      this.objData = dataObj.data;
    });
  }

  getData(id, user) {
    this.router.navigate(['/data/' + user + '/' + id]);
  }

  editDevice(id, usr, lat, lng, name, place) {
    console.log(id + '' + usr);
    this.dialog.open(DialogEditComponent, {  height: '800px',
                                            width: '900px',
                                            data: {Id: id, Usr: usr, Lat: lat, Lng: lng, Name: name, Place: place}});
  }


  deleteDevice(id, usr) {
    const status = confirm('Do you really want to delete this information?');
    const httpOptions = {
      headers: new HttpHeaders({
           'Content-Type' : 'application/json;charset=utf-8'
      })
    };
    if ( status === true) {
      this.http.delete(environment.apiUrl + '/device/' + id, httpOptions ).subscribe( data => {
        const payload = JSON.parse(JSON.stringify(data));
        if (payload.idStatus === 200) {
          this.router.navigateByUrl('/devices', {skipLocationChange : true});
        } else {
          alert('Error');
        }

      });
    }
  }

}
