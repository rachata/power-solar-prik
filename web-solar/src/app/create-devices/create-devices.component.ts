import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { environment } from 'src/environments/environment';


export interface FormModel {
  name: string;
  place: string;
  lat: number;
  lng: number;
  idUser: number;
}

@Component({
  selector: 'app-create-devices',
  templateUrl: './create-devices.component.html',
  styleUrls: ['./create-devices.component.css']
})

export class CreateDevicesComponent implements OnInit {


  formData: FormData = new FormData();
  formGroup: FormGroup = this.createForm({
    name: '',
    place: '',
    lat: 0,
    lng: 0,
    idUser: 1,
  });

  txtLat: number;
  txtLng: number;

  lat = 7.1545593;
  lng = 100.6223298;
  zoom = 15;
  markers: Array<{ lat: number, lng: number }> = [];

  constructor(private http: HttpClient, private form: FormBuilder) {
  }

  ngOnInit() {

  }

  private createForm(model: FormModel): FormGroup {
    return this.form.group(model);
  }

  postData() {

    const body = this.formGroup.value;
    this.http.post(environment.apiUrl + '/device', body).subscribe(data => {
      const status = JSON.parse(JSON.stringify(data));
      if (status.idStatus === 200) {
        alert('Save Data Success');
      } else {
        alert('Error');
      }
    });
  }

  markMap(e) {
    console.log(e);
    if (this.markers.length > 0) {
      this.markers.pop();
    }
    this.markers.push({ lat: e.coords.lat, lng: e.coords.lng });
    this.formGroup.value.lat = e.coords.lat;
    this.formGroup.value.lng = e.coords.lng;
    this.txtLat = e.coords.lat;
    this.txtLng = e.coords.lng;
  }

}
