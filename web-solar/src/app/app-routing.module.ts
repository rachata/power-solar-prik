import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListDeviceComponent } from './list-device/list-device.component';
import { ListDataDeviceComponent } from './list-data-device/list-data-device.component';
import { GoogleMapComponent } from './google-map/google-map.component';
import { CreateDevicesComponent } from './create-devices/create-devices.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
 {path : 'devices', component: ListDeviceComponent},
 {path : 'data/:usr/:id', component: ListDataDeviceComponent},
 {path : 'viewmaps', component : GoogleMapComponent},
 {path : 'create', component : CreateDevicesComponent},
 {path : 'register', component : RegisterComponent},
 {path : 'login', component: LoginComponent},
 {path : ' ', component : GoogleMapComponent},
 {path : '**' , component : GoogleMapComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
