import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainNavComponent } from './main-nav/main-nav.component';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MatNativeDateModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { HomeComponent } from './home/home.component';
import { ListDeviceComponent } from './list-device/list-device.component';
import { HttpClientModule } from '@angular/common/http';
import { ListDataDeviceComponent } from './list-data-device/list-data-device.component';
import { ViewChartComponent } from './view-chart/view-chart.component';
import { ChartModule } from 'angular2-chartjs';
import { GoogleMapComponent } from './google-map/google-map.component';

import { AgmCoreModule } from '@agm/core';
import { DialogMarkersComponent } from './dialog-markers/dialog-markers.component';
import { CreateDevicesComponent } from './create-devices/create-devices.component';
import { DialogEditComponent } from './dialog-edit/dialog-edit.component';
import { DialogStatusComponent } from './dialog-status/dialog-status.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    HomeComponent,
    ListDeviceComponent,
    ListDataDeviceComponent,
    ViewChartComponent,
    GoogleMapComponent,
    DialogMarkersComponent,
    CreateDevicesComponent,
    DialogEditComponent,
    DialogStatusComponent,
    RegisterComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatNativeDateModule,
    ChartModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCIa7TFIuyxPuLF6UW6OW9nZ1Jw3s0cEZM'
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents : [
    ViewChartComponent,
    DialogMarkersComponent,
    DialogEditComponent,
    DialogStatusComponent
  ]
})
export class AppModule { }
