import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DialogEditComponent } from '../dialog-edit/dialog-edit.component';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dialog-status',
  templateUrl: './dialog-status.component.html',
  styleUrls: ['./dialog-status.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class DialogStatusComponent implements OnInit {

  txtName: string;
  txtPlace: string;


  constructor(private matDialogRef: MatDialogRef<DialogEditComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private http: HttpClient,
              private router: Router) {

                this.txtName = data.Name;
                this.txtPlace = data.Place;

               }

  ngOnInit() {

  }

  viewData() {
    this.matDialogRef.close();
    this.router.navigate(['/data/' + this.data.Usr + '/' + this.data.Device]);
  }

}
