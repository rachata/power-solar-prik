import { Component, OnInit } from '@angular/core';
import { sha256 } from 'js-sha256';
import { FormGroup, FormBuilder, NgModel } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export interface Register {
  fullName: string;
  username: string;
  password: string;
  repassword: string;
}

export interface RegisModel {
  fullName: string;
  username: string;
  password: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formData: FormData = new FormData();
  formGroup: FormGroup = this.createForm({
    fullName: '',
    username: '',
    password: '',
    repassword: ''
  });

  constructor(private form: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    console.log(sha256.hex('aaaaaaaa'));
  }

  private createForm(model: Register): FormGroup {
    return this.form.group(model);
  }

  createUser() {
    console.log(this.formGroup);
    console.log(this.formGroup.value.password.length);
    if (this.formGroup.value.password === this.formGroup.value.repassword) {
      if (this.formGroup.value.password.length < 8) {
        alert('Password not 8 characters');
      } else {
        const hash = sha256.hex(this.formGroup.value.password);

        this.http.post<RegisModel>(environment.apiUrl + '/user', { password: hash,
                                                                   username: this.formGroup.value.username,
                                                                   fullName: this.formGroup.value.fullName }).subscribe( data => {
          console.log(data);
        });
      }
    } else {
      alert('Password Not Match');
    }
  }

}
