import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogMarkersComponent } from './dialog-markers.component';

describe('DialogMarkersComponent', () => {
  let component: DialogMarkersComponent;
  let fixture: ComponentFixture<DialogMarkersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogMarkersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogMarkersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
