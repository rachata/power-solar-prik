import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterPage } from '../register/register';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({ 

  
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public form: FormGroup;
  constructor(public storage : Storage , public httpClient : HttpClient ,  private toastCtrl: ToastController , private formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.form = this.formBuilder.group({
      username: ['',Validators.required],
      password : ['', Validators.required]
    });
  
  }
  register(){
    this.navCtrl.push(RegisterPage);
  }

  loginForm(){

    var user = this.form.value.username;
    var pass = this.form.value.password;

    let body = 
      {
        "username" : user,
        "password" : pass
      }
    

    let httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',

    }); 

    let options = {
      headers: httpHeaders
    }; 


    var res = this.httpClient.post('http://167.71.222.155:8080/api/user/login' , body , options);
    res
      .subscribe(data => {

        if(data['idStatus'] == 200){

          this.storage.set('idUser', data['id']);
          console.log(JSON.stringify(data));


          this.navCtrl.setRoot(TabsPage);
        }else{

          const toast = this.toastCtrl.create({
            message: '  Username หรือ รหัสผ่านผิด',
            duration: 3000
          });
          toast.present();

        }
    })
  }

}
