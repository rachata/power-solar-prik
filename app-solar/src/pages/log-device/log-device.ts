import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the LogDevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-log-device',
  templateUrl: 'log-device.html',
})
export class LogDevicePage {
  title : String;
  logs : Observable<any>;
  item : any;
  constructor(public navCtrl: NavController, public navParams: NavParams ,  public httpClient: HttpClient) {
    var idDevice = this.navParams.get('data')['idDevice'];

    this.logs = this.httpClient.get('https://128.199.107.134:8181/api/device/log/'+idDevice+"/1");
    this.logs
    .subscribe(data => {
      this.item = data['data'];
      console.log(JSON.stringify(this.item))
    })


  }

  ionViewDidLoad() {
    
    this.title = this.navParams.get('data')['name']
  }

}
