import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogDevicePage } from './log-device';

@NgModule({
  declarations: [
    LogDevicePage,
  ],
  imports: [
    IonicPageModule.forChild(LogDevicePage),
  ],
})
export class LogDevicePageModule {}
