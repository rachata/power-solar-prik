import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapDevicePage } from './map-device';

@NgModule({
  declarations: [
    MapDevicePage,
  ],
  imports: [
    IonicPageModule.forChild(MapDevicePage),
  ],
})
export class MapDevicePageModule {}
