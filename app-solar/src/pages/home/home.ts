import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SubDevicePage } from '../sub-device/sub-device';
import { Storage } from '@ionic/storage';
declare var google;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  devices: Observable<any>;
  item = [];

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  stateMark: boolean = false
  marker: any;
  mapState = false;

  markerArray = []

  idUser : any
  constructor(public storage : Storage , private screenOrientation: ScreenOrientation, public navCtrl: NavController, public httpClient: HttpClient) {
    // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  
  
    this.storage.get('idUser').then((val) => {
      this.idUser = val;
    });



  }

  ionViewDidLoad() {

    this.storage.get('idUser').then((val) => {
      this.devices = this.httpClient.get('http://167.71.222.155:8080/api/device/'+val);
      this.devices
        .subscribe(data => {
          this.item = (data['data']);
  
          this.loadMap(this.item);
        })
  
    });




   

  }

  loadMap(item) {


    console.log(JSON.stringify(item))


    var m = []


    if(item != null){
      for (var i = 0; i < item.length; i++) {

        var temp = {}
        var latlng = {}
  
  
        latlng['lat'] = item[i]['lat']
        latlng['lng'] = item[i]['lng']
        temp['latlng'] = latlng
        temp['title'] = item[i]['name']
        temp['idDevice'] = item[i]['idDevice']
  
        m.push(temp)
      }
  
  
  
      if (this.mapState == false) {
        let latLng = new google.maps.LatLng(7.1545593, 100.6223298);
  
        let mapOptions = {
          center: latLng,
          zoom: 6,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
  
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.mapState = true;
      }
  
  
      this.markerArray = [];
      var markers = [];
      for (var i = 0; i < m.length; i++) {
  
   
        console.log(m[i]['title']);
        markers[i] = new google.maps.Marker({
          map: this.map,
          title: m[i]['title'],
          animation: google.maps.Animation.DROP,
          position: m[i]['latlng']
        });
  
        this.markerArray.push(markers[i])
        this.stateMark = true;
  
        google.maps.event.addListener( markers[i], 'click', ( 
          function( marker, i  , navCtrl) {
            return function() {
              console.log(JSON.stringify(m[i]['latlng']));
              console.log(m[i]['idDevice'])
  
              navCtrl.push(SubDevicePage , {device : m[i]['idDevice']})
         
            }
          }
        )( markers[i], i  , this.navCtrl) );
  
  
      }
    }
    

  }

  ionViewDidEnter() {

    console.log("onViewDidEnter home")


    if (this.stateMark == true || this.marker != null) {
      this.stateMark = false;

      for(var i = 0; i < this.markerArray.length; i++){
        this.markerArray[i].setMap(null);
      }
    }

    this.storage.get('idUser').then((val) => {
      this.devices = this.httpClient.get('http://167.71.222.155:8080/api/device/'+val);
      this.devices
        .subscribe(data => {
          this.item = (data['data']);
  
          this.loadMap(this.item)
  
        })
    });



   

  }


}


