import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DatePicker } from '@ionic-native/date-picker';
import { GraphMainPage } from '../graph-main/graph-main';

/**
 * Generated class for the AcDatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ac-date',
  templateUrl: 'ac-date.html',
})
export class AcDatePage {

  datetimeStart : any;
  datetimeEnd : any;
  dateDatetime: FormGroup
  graphData :Observable<any>;
  powerX = [];
  powerY = [];
  
  constructor(private toastCtrl: ToastController  , private httpClient : HttpClient ,  private datePicker: DatePicker, public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit() {
    this.dateDatetime = new FormGroup({
      'startDate': new FormControl(null, Validators.required),
      'endDate': new FormControl(null, Validators.required)
      

    });

  }

  startTime() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      is24Hour: true,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {

        var m;
        if(date.getMonth() == 12){
          m = 1;
        }else{
          m = date.getMonth()  + 1;
        }

       this.datetimeStart = date.getFullYear()+'-'+m+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()
       this.dateDatetime.patchValue({
        'startDate': this.datetimeStart 
        
      });
       console.log('Got date: ', this.datetimeStart)
       
      },
      err => {
        console.log('Error occurred while getting date: ', err)
      }
    );
  }

  endTime() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      is24Hour: true,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {

        var m;
        if(date.getMonth() == 12){
          m = 1;
        }else{
          m = date.getMonth() + 1;
        }
       this.datetimeEnd = date.getFullYear()+'-'+m+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()
       this.dateDatetime.patchValue({
        'endDate': this.datetimeEnd 
        
      }); 
       console.log('Got date: ', this.datetimeEnd)
       
      },
      err => {
        console.log('Error occurred while getting date: ', err)
      }
    );
  }

  onSubmit(){
    var start = this.dateDatetime.value.startDate
    var end = this.dateDatetime.value.endDate

    console.log("start : " + start)
    console.log("end : " + end)

    this.callAPI(start , end);

  }

  callAPI(strat : any , end : any){


    var idDevice = this.navParams.get("idDevice");

    var url = "http://167.71.222.155:8080/api/log/ac/"+idDevice+"/"+strat+"/"+end;

    console.log(url)
    this.graphData = this.httpClient.get(url);
    this.graphData
      .subscribe(data => {

        console.log(JSON.stringify(data));


        if(data['idStatus'] == 200){

          var phase1 = data['phase1']
          var phase2 = data['phase2']
          var phase3 = data['phase3']
          var date = data['dt']
          console.log(phase1)
          this.navCtrl.push(GraphMainPage , {phase1 :phase1 , phase2 : phase2   , phase3 : phase3  , date : date })

       


        }else{
          let toast = this.toastCtrl.create({
            message: data['nameStatus'],
            duration: 3000,
            position: 'top'
          });
        
         
        
          toast.present();
        }
       
      },
        error => {
          console.log(JSON.stringify(error))
        })

  }
  

}
