import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcDatePage } from './ac-date';

@NgModule({
  declarations: [
    AcDatePage,
  ],
  imports: [
    IonicPageModule.forChild(AcDatePage),
  ],
})
export class AcDatePageModule {}
