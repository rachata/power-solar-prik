import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import { LogDevicePage } from '../log-device/log-device';
import { AddDevicePage } from '../add-device/add-device';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { SubDevicePage } from '../sub-device/sub-device';
import { GraphMainPage } from '../graph-main/graph-main';
import { AcDatePage } from '../ac-date/ac-date';
import { FormGroup, FormControl, Validators } from '@angular/forms';

/**
 * Generated class for the DevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-device',
  templateUrl: 'device.html',
})
export class DevicePage implements OnInit {

  devices: Observable<any>;
  delete: Observable<any>;
 
  item = [];

  page = LogDevicePage
  idUser : any
  timeInterval :any
  constructor(public storage : Storage ,  private launchNavigator : LaunchNavigator,  private alertCtrl : AlertController,  public navCtrl: NavController, public navParams: NavParams,  public httpClient: HttpClient) {
    


     
    this.storage.get('idUser').then((val) => {
      this.idUser = val;

      this.devices = this.httpClient.get('http://167.71.222.155:8080/api/device/'+this.idUser);
      this.devices
        .subscribe(data => {
          this.item = (data['data']);


        })

    });






      

    }

    ngOnInit(){




    this.timeInterval =  setInterval(() => {
      console.log("ewf")
      this.storage.get('idUser').then((val) => {
        this.idUser = val;
  
        this.devices = this.httpClient.get('http://167.71.222.155:8080/api/device/'+this.idUser);
        this.devices
          .subscribe(data => {
            this.item = (data['data']);
  
  
          })
  
      });
    }, 3000);


    }

    ionViewWillLeave(){
      clearInterval( this.timeInterval);
  }



    addDevice(){
      
      this.navCtrl.push(AddDevicePage , {title : "Add Device"})
    }


     logDevice(index: any) {
      this.navCtrl.push(this.page, { data: this.item[index] })
    }


     graph(device: any) {


      this.navCtrl.push(AcDatePage , {idDevice : device['idDevice']})
     
   
    }

    editDevice(device){

      this.navCtrl.push(AddDevicePage , {title : "Edit Device" , data : device})
      
    }

    deleteDevice(device){


      let alert = this.alertCtrl.create({
        title: 'คำเตือน',
        message: 'คุณต้องการลบอุปกรณ์ ' + device['name'] + " ใช่หรือไม่",
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'OK',
            handler: () => {
              this.okDelete(device)
            }
          }
        ]
      });
      alert.present();


    }


    okDelete(device){

      this.delete = this.httpClient.delete('http://167.71.222.155:8080/api/device/'+device['idDevice']);
      this.delete
        .subscribe(data => {
          
          console.log(JSON.stringify(data))
          if(data['idStatus'] == 200){
            this.devices = this.httpClient.get('http://167.71.222.155:8080/api/device/'+this.idUser);
            this.devices
              .subscribe(data => {
                this.item = (data['data']);
      
              })
          }

        })


    }

    gotoSub(idDevice){
      console.log(idDevice);
      this.navCtrl.push(SubDevicePage , {device : idDevice});

    }
    gotoMap(device){

      console.log(JSON.stringify(device))

      this.launchNavigator.navigate([7, 100]);;
    }


    ionViewDidEnter(){
      this.devices = this.httpClient.get('http://167.71.222.155:8080/api/device/'+this.idUser);
      this.devices
        .subscribe(data => {
          this.item = (data['data']);

        })

    }







  }
