import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
/**
 * Generated class for the GraphMainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-graph-main',
  templateUrl: 'graph-main.html',
})
export class GraphMainPage  implements OnInit{

  colorBrown = "#a37207";
  colorBlack = "#030200";
  colorGray = "#706f6d";
  colorDC = "#e8651a";
  colorPoint = "#FFFFFF";
  @ViewChild('lineCanvasPower') lineCanvasPower;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GraphMainPage');
  }

  ngOnInit(){
    var phase1 = this.navParams.get("phase1")
    var phase2 = this.navParams.get("phase2")
    var phase3 = this.navParams.get("phase3")
    var date = this.navParams.get("date")

    this.lineChartPowerMethod(phase1 , phase2 , phase3 , date) ;
  }


  lineChartPowerMethod(phase1:any , phase2:any , phase3:any , date: any) {


    var line = new Chart(this.lineCanvasPower.nativeElement, {
      type: 'line',
      data: {

        labels: date,
        datasets: [
          {
            label: 'AC Phase 1',
            fill: false,
            lineTension: 0.1,
            backgroundColor: this.colorBrown,
            borderColor: this.colorBrown,
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',

            pointBackgroundColor: '#fff',
            pointBorderWidth: 7,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: this.colorBrown,
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: phase1,
            spanGaps: false,
          },


        {
          label: 'AC Phase 2',
          fill: false,
          lineTension: 0.1,
          backgroundColor: this.colorBlack,
          borderColor: this.colorBlack,
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',

          pointBackgroundColor: '#fff',
          pointBorderWidth: 7,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: this.colorBlack,
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: phase2,
          spanGaps: false,
        },

        {
          label: 'AC Phase 3',
          fill: false,
          lineTension: 0.1,
          backgroundColor: this.colorGray,
          borderColor: this.colorGray,
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',

          pointBackgroundColor: '#fff',
          pointBorderWidth: 7,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: this.colorGray,
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: phase3,
          spanGaps: false,
        }



          
        ] 

      },
      options: {
        tooltips: {
          mode: 'index',
          intersect: false
        },

        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }],
          xAxes: [{
            ticks: {

              maxTicksLimit: 3,
              autoSkip: true
            },
            display: true
          }],

        }
      }
    });
  }

}
