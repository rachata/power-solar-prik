import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraphMainPage } from './graph-main';

@NgModule({
  declarations: [
    GraphMainPage,
  ],
  imports: [
    IonicPageModule.forChild(GraphMainPage),
  ],
})
export class GraphMainPageModule {}
