import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the AddDevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-add-device',
  templateUrl: 'add-device.html',
})
export class AddDevicePage implements OnInit {
  addDevice: FormGroup

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  marker = null;
  res: Observable<any>;
  stateMark = false;
  param : any;
  title:any
  btnText:any;
  dataEdit:any
  idUser  : any;
  constructor(public storage : Storage , private toastCtrl : ToastController ,  private httpClient : HttpClient ,  public navCtrl: NavController, public navParams: NavParams) {
  
  
    this.storage.get('idUser').then((val) => {
      this.idUser = val;
    });

    this.param = this.navParams.data;
    this.dataEdit = this.param['data'];
    console.log(JSON.stringify(this.dataEdit))
    this.title = this.param['title']
    if(this.title == "Add Device"){
      this.btnText = "สร้าง"
    }else{
      this.btnText = "บันทึก"
    }
    
  
  }


  ngOnInit() {

    if(this.title == "Add Device"){
      this.addDevice = new FormGroup({
        'nameDevice': new FormControl(null, Validators.required),
        'placeDevice': new FormControl(null, Validators.required),
        'latDevice': new FormControl(null, Validators.required),
        'lngDevice': new FormControl(null, Validators.required)
  
      });
    }else{

      this.addDevice = new FormGroup({
        'nameDevice': new FormControl(this.dataEdit['name'], Validators.required),
        'placeDevice': new FormControl(this.dataEdit['place'], Validators.required),
        'latDevice': new FormControl(this.dataEdit['lat'], Validators.required),
        'lngDevice': new FormControl(this.dataEdit['lng'], Validators.required)
  
      });
    }
   

  }

  ionViewDidLoad() {
    let latLng = new google.maps.LatLng(7.1545593, 100.6223298);

    let mapOptions = {
      center: latLng,
      zoom: 6,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);


    if(this.title == "Edit Device"){

      let latLng = new google.maps.LatLng(this.dataEdit['lat'], this.dataEdit['lng']);

      this.marker = new google.maps.Marker({
        map : this.map,
        animation: google.maps.Animation.DROP,
        position: latLng,
        draggable: true
       
    });

    }
    google.maps.event.addListener(this.map, 'click', (event)=>{

        this.addMarker(event)
    })

  }


  public addMarker(event) {

    if(this.stateMark == true || this.marker != null){
      this.marker.setMap(null);
      this.stateMark = false;
    }
   
    this.marker = new google.maps.Marker({
        map : this.map,
        animation: google.maps.Animation.DROP,
        position: event.latLng,
        draggable: true
       
    });

    this.stateMark = true;
    var temp = JSON.stringify(event);
    var data = JSON.parse(temp);


    this.addDevice.patchValue({
      'latDevice': data['latLng']['lat'], 
      'lngDevice': data['latLng']['lng']
    });

  }


  onSubmit() {

    if(this.title == "Add Device"){
      var nameDevice = this.addDevice.value.nameDevice
      var placeDevice = this.addDevice.value.placeDevice
      var latDevice = this.addDevice.value.latDevice
      var lngDevice = this.addDevice.value.lngDevice
  
  
      let body = {
        "name" : nameDevice ,
        "place" : placeDevice,
        "lat" : latDevice,
        "lng" : lngDevice,
        "idUser" : this.idUser
      }
  
      let httpHeaders = new HttpHeaders({
        'Content-Type' : 'application/json',
  
      }); 
  
      let options = {
        headers: httpHeaders
      }; 
  
  
      this.res = this.httpClient.post('http://167.71.222.155:8080/api/device' , body , options);
      this.res
        .subscribe(data => {
  
          if(data['idStatus'] == 200){
  
            this.addDevice.reset();
            const toast = this.toastCtrl.create({
              message: 'เพิ่มอุปกรณ์สำเร็จ',
              duration: 3000
            });
            toast.present();
  
          }else{
  
            const toast = this.toastCtrl.create({
              message: 'เพิ่มอุปกรณ์ไม่สำเร็จ :' + data['nameStatus'],
              duration: 3000
            });
            toast.present();
  
          }
      })
    }else{
      var nameDevice = this.addDevice.value.nameDevice
      var placeDevice = this.addDevice.value.placeDevice
      var latDevice = this.addDevice.value.latDevice
      var lngDevice = this.addDevice.value.lngDevice
  


      console.log(nameDevice)
  
      let body = {
        "name" : nameDevice ,
        "place" : placeDevice,
        "lat" : latDevice,
        "lng" : lngDevice,
        "idDevice" : this.dataEdit['idDevice']
      }
  
      let httpHeaders = new HttpHeaders({
        'Content-Type' : 'application/json',
  
      }); 
  
      let options = {
        headers: httpHeaders
      }; 

      this.res = this.httpClient.put('http://167.71.222.155:8080/api/device' , body , options);
      this.res
        .subscribe(data => {
  
          
          
          if(data['idStatus'] == 200){
  
           
            const toast = this.toastCtrl.create({
              message: 'แก้ไขอุปกรณ์สำเร็จ',
              duration: 3000
            });
            toast.present();
  
          }else{
  
            const toast = this.toastCtrl.create({
              message: 'แก้ไขอุปกรณ์ไม่สำเร็จ :' + data['nameStatus'],
              duration: 3000
            });
            toast.present();
  
          }
      })

    }
  }
}
