import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { DatePicker } from '@ionic-native/date-picker';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GraphPage } from '../graph/graph';

/**
 * Generated class for the SelectDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-data',
  templateUrl: 'select-data.html',
})
export class SelectDataPage implements OnInit {

  datetimeStart : any;
  datetimeEnd : any;
  dateDatetime: FormGroup
  graphData :Observable<any>;
  powerX = [];
  powerY = [];
  constructor(private toastCtrl: ToastController , private httpClient : HttpClient ,  private datePicker: DatePicker, public navCtrl: NavController, public navParams: NavParams) {
  }


  ngOnInit() {
    this.dateDatetime = new FormGroup({
      'startDate': new FormControl(null, Validators.required),
      'endDate': new FormControl(null, Validators.required)
      

    });
  }

  startTime() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      is24Hour: true,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {

        var m;
        if(date.getMonth() == 12){
          m = 1;
        }else{
          m = date.getMonth()  + 1;
        }

       this.datetimeStart = date.getFullYear()+'-'+m+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()
       this.dateDatetime.patchValue({
        'startDate': this.datetimeStart 
        
      });
       console.log('Got date: ', this.datetimeStart)
       
      },
      err => {
        console.log('Error occurred while getting date: ', err)
      }
    );
  }

  endTime() {
    this.datePicker.show({
      date: new Date(),
      mode: 'datetime',
      is24Hour: true,
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {

        var m;
        if(date.getMonth() == 12){
          m = 1;
        }else{
          m = date.getMonth() + 1;
        }
       this.datetimeEnd = date.getFullYear()+'-'+m+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()
       this.dateDatetime.patchValue({
        'endDate': this.datetimeEnd 
        
      }); 
       console.log('Got date: ', this.datetimeEnd)
       
      },
      err => {
        console.log('Error occurred while getting date: ', err)
      }
    );
  }

  onSubmit(){
    var start = this.dateDatetime.value.startDate
    var end = this.dateDatetime.value.endDate

    console.log("start : " + start)
    console.log("end : " + end)

    this.callAPI(start , end);

  }

  callAPI(strat : any , end : any){

    var id = this.navParams.get("idSub");
    var url = "http://167.71.222.155:8080/api/log/"+id+"/"+strat+"/"+end;

    console.log(url)
    this.graphData = this.httpClient.get(url);
    this.graphData
      .subscribe(data => {
       

        if(data['idStatus'] == 200){
          var key = Object.keys(data['data']);
       


          var powerStr = JSON.stringify(data['data']);
          var power = JSON.parse(powerStr);
  
          for(var x = 0; x< key.length; x++){
           
            this.powerX.push(power[x]['dt']);
          }
  
          for(var  y = 0; y< key.length; y++){
            this.powerY.push(power[y]['power']);
          }
  
          console.log(JSON.stringify(this.powerX));
          console.log(JSON.stringify(this.powerY));

          var id = this.navParams.get("idSub");
          var phase = this.navParams.get("phase")

          this.navCtrl.push(GraphPage , {x : this.powerX , y : this.powerY , id : id , phase : phase});
        }else{

          let toast = this.toastCtrl.create({
            message: data['nameStatus'],
            duration: 3000,
            position: 'top'
          });
        
         
        
          toast.present();
        }

        

       
      },
        error => {
          console.log(JSON.stringify(error))
        })

  }


}
