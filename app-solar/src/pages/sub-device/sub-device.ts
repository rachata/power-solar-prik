import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { SelectDataPage } from '../select-data/select-data';
import { FormGroup, FormControl, Validators } from '@angular/forms';

/**
 * Generated class for the SubDevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sub-device',
  templateUrl: 'sub-device.html',
})
export class SubDevicePage implements OnInit {

  subDevice: Observable<any>;
  device: any

  item: any;
  addData: FormGroup
  timeInterval : any
  idUser:any
  constructor(  private toastCtrl : ToastController , public storage : Storage ,  public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient) {
    this.device = this.navParams.get("device");
 
  
    this.storage.get('idUser').then((val) => {
      this.idUser = val;
    });

  
  }




  gotoGraph(device){

    this.navCtrl.push(SelectDataPage , {idSub : device['id'] , phase : device['phase']})
    console.log(JSON.stringify(device));

  }

  ngOnInit() {

    this.addData = new FormGroup({
      'acPhase1Vol': new FormControl(0.0, Validators.required),
      'acPhase2Vol': new FormControl(0.0, Validators.required),
      'acPhase3Vol': new FormControl(0.0, Validators.required),
      'dcVol': new FormControl(0.0, Validators.required),
      'acPhase1Amp': new FormControl(0.0, Validators.required),
      'acPhase2Amp': new FormControl(0.0, Validators.required),
      'acPhase3Amp': new FormControl(0.0, Validators.required),
      'dcAmp': new FormControl(0.0, Validators.required),

    });

    var url;
    this.storage.get('idUser').then((val) => {
      url =  "http://167.71.222.155:8080/api/sub-device/"+val+"/" + this.device
      this.subDevice = this.httpClient.get(url);
      this.subDevice
        .subscribe(data => {
          this.item = data['data']
          console.log(JSON.stringify(this.item))
  
        },
          error => {
            console.log(JSON.stringify(error))
          })
    });

 



    this.timeInterval =  setInterval(() => {
      var url = "http://167.71.222.155:8080/api/sub-device/"+this.idUser+"/" + this.device

      console.log(url)
      this.subDevice = this.httpClient.get(url);
      this.subDevice
        .subscribe(data => {
          this.item = data['data']
          console.log(JSON.stringify(this.item))

        },
          error => {
            console.log(JSON.stringify(error))
            
          })
    }, 3000);
  }


  ionViewWillLeave(){
      clearInterval( this.timeInterval);
  }


  onSubmit() {


    var acPhase1Vol = this.addData.value.acPhase1Vol
    var acPhase2Vol = this.addData.value.acPhase2Vol
    var acPhase3Vol = this.addData.value.acPhase3Vol
    var dcVol = this.addData.value.dcVol


    var acPhase1Amp = this.addData.value.acPhase1Amp
    var acPhase2Amp = this.addData.value.acPhase2Amp
    var acPhase3Amp = this.addData.value.acPhase3Amp
    var dcAmp = this.addData.value.dcAmp

    var url = "http://167.71.222.155:8080/api/sub-device/id/" + this.device

    console.log(url)
    this.subDevice = this.httpClient.get(url);
    this.subDevice
      .subscribe(data => {
       
        console.log(JSON.stringify(data))


        if(data['idStatus'] == 200){


            var urlRealtime = "http://167.71.222.155:8080/api/sub-device";


           data = [

             {
              id : 28,
              realV : acPhase1Vol,
              realA : acPhase1Amp
            },
            {
              id : 29,
              realV : acPhase2Vol,
              realA : acPhase2Amp
            },
            {
              id : 30,
              realV : acPhase3Vol,
              realA : acPhase3Amp
            },
            {
              id : 31,
              realV : dcVol,
              realA : dcAmp
            }
            ]

            
            for(var i = 0; i < data.length; i++){

             

              console.log("wef");
          
              let httpHeaders = new HttpHeaders({
                'Content-Type' : 'application/json',
          
              }); 
          
              let options = {
                headers: httpHeaders
              }; 
        
             var a =  this.httpClient.put("http://167.71.222.155:8080/api/sub-device" ,data[i] , options);

             a.subscribe(data=>{

             },
             error => {
              console.log(JSON.stringify(error))
              
            });
            }


        }else{
          const toast = this.toastCtrl.create({
            message: 'ผิดพลาด',
            duration: 3000
          });
          toast.present();
        }

      },
        error => {
          console.log(JSON.stringify(error))
          
        })

    




  }



}
