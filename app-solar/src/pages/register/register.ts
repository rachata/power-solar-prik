import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public form : FormGroup;

  constructor(public httpClient : HttpClient ,  public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,private toastCtrl: ToastController) {

    this.form = this.formBuilder.group({
        fullname : ['', Validators.required],
        username:['',Validators.required],
        password:['',Validators.required],
        repassword:['',Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  createRegis(){
   
  
    var user = this.form.value.username;
    var pass = this.form.value.password;
    var fullname = this.form.value.fullname;
    var repassword = this.form.value.repassword

    if(pass == repassword){


      let body = 
      {
        "fullName" : fullname,
        "username" : user,
        "password" : pass
      }
    

    let httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',

    }); 

    let options = {
      headers: httpHeaders
    }; 


    var res = this.httpClient.post('http://167.71.222.155:8080/api/user/' , body , options);
    res
      .subscribe(data => {
console.log(JSON.stringify(data))
        if(data['idStatus'] == 200){

      
          const toast = this.toastCtrl.create({
            message: 'สมัครสมาชิกสำเร็จ',
            duration: 3000
          });
          toast.present();

          this.navCtrl.pop();



        }else{

          const toast = this.toastCtrl.create({
            message: 'ผิดพลาด',
            duration: 3000
          });
          toast.present();

        }
    })
    
    }else{

      const toast = this.toastCtrl.create({
        message: 'รหัสผ่านไม่ตรงกัน',
        duration: 3000
      });
      toast.present();
    }


    
  


   
  }

}
