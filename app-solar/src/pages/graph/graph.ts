import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
/**
 * Generated class for the GraphPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-graph',
  templateUrl: 'graph.html',
})
export class GraphPage implements OnInit {
  @ViewChild('lineCanvasVol') lineCanvasVol;
  @ViewChild('lineCanvasAmp') lineCanvasAmp;
  @ViewChild('lineCanvasPower') lineCanvasPower;


  powerX = [];
  powerY = [];

  idSub
  phase;

  colorBrown = "#a37207";
  colorBlack = "#030200";
  colorGray = "#706f6d";
  colorDC = "#e8651a";
  colorPoint = "#FFFFFF";

  colorLine: any;
  graphData: Observable<any>;
  constructor(private httpClient: HttpClient, public navCtrl: NavController, public navParams: NavParams) {


  }

  ionViewDidLoad() {

  }

  ngOnInit() {


    this.idSub = this.navParams.get("id");
    this.phase = this.navParams.get("phase");

    this.powerX = this.navParams.get("x");
    this.powerY = this.navParams.get("y");

    if (this.phase == 1) {
      this.colorLine = this.colorBrown;
    } else if (this.phase == 2) {
      this.colorLine = this.colorBlack;
    } else if (this.phase == 3) {
      this.colorLine = this.colorGray;
    } else {
      this.colorLine = this.colorDC;
    }
    this.lineChartPowerMethod();
    console.log(this.phase + " " + this.powerX + " " + this.powerY);


  }

  lineChartPowerMethod() {


    var line = new Chart(this.lineCanvasPower.nativeElement, {
      type: 'line',
      data: {

        labels: this.powerX,
        datasets: [
          {
            label: 'กำลังไฟฟ้า',
            fill: false,
            lineTension: 0.1,
            backgroundColor: this.colorLine,
            borderColor: this.colorLine,
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',

            pointBackgroundColor: '#fff',
            pointBorderWidth: 7,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: this.colorLine,
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.powerY,
            spanGaps: false,
          }
        ]
      },
      options: {
        tooltips: {
          mode: 'index',
          intersect: false
        },

        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }],
          xAxes: [{
            ticks: {

              maxTicksLimit: 3,
              autoSkip: true
            },
            display: true
          }],

        }
      }
    });
  }

}
