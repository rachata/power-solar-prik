import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GraphPage } from '../pages/graph/graph';
import { LogDevicePage } from '../pages/log-device/log-device';
import { DevicePage } from '../pages/device/device';
import { AddDevicePage } from '../pages/add-device/add-device';

import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LaunchNavigator} from '@ionic-native/launch-navigator'
import { SubDevicePage } from '../pages/sub-device/sub-device';
import { IonicStorageModule } from '@ionic/storage';
import {ScreenOrientation} from '@ionic-native/screen-orientation'
import { GraphMainPage } from '../pages/graph-main/graph-main';

import { DatePicker} from '@ionic-native/date-picker'
import { SelectDataPage } from '../pages/select-data/select-data';
import { AcDatePage } from '../pages/ac-date/ac-date';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
 @NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    GraphPage,
    LogDevicePage,
    DevicePage,
    AddDevicePage,
    SubDevicePage,
    GraphMainPage,
    SelectDataPage,
    AcDatePage,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    GraphPage,
    LogDevicePage,
    DevicePage,
    AddDevicePage,
    SubDevicePage,
    GraphMainPage,
    SelectDataPage,
    AcDatePage,
    LoginPage,
    RegisterPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    LaunchNavigator,
    ScreenOrientation,
    DatePicker,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
