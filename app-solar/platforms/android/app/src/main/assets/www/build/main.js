webpackJsonp([11],{

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcDatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_date_picker__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__graph_main_graph_main__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AcDatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AcDatePage = (function () {
    function AcDatePage(toastCtrl, httpClient, datePicker, navCtrl, navParams) {
        this.toastCtrl = toastCtrl;
        this.httpClient = httpClient;
        this.datePicker = datePicker;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.powerX = [];
        this.powerY = [];
    }
    AcDatePage.prototype.ngOnInit = function () {
        this.dateDatetime = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormGroup */]({
            'startDate': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required),
            'endDate': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required)
        });
    };
    AcDatePage.prototype.startTime = function () {
        var _this = this;
        this.datePicker.show({
            date: new Date(),
            mode: 'datetime',
            is24Hour: true,
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(function (date) {
            var m;
            if (date.getMonth() == 12) {
                m = 1;
            }
            else {
                m = date.getMonth() + 1;
            }
            _this.datetimeStart = date.getFullYear() + '-' + m + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
            _this.dateDatetime.patchValue({
                'startDate': _this.datetimeStart
            });
            console.log('Got date: ', _this.datetimeStart);
        }, function (err) {
            console.log('Error occurred while getting date: ', err);
        });
    };
    AcDatePage.prototype.endTime = function () {
        var _this = this;
        this.datePicker.show({
            date: new Date(),
            mode: 'datetime',
            is24Hour: true,
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(function (date) {
            var m;
            if (date.getMonth() == 12) {
                m = 1;
            }
            else {
                m = date.getMonth() + 1;
            }
            _this.datetimeEnd = date.getFullYear() + '-' + m + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
            _this.dateDatetime.patchValue({
                'endDate': _this.datetimeEnd
            });
            console.log('Got date: ', _this.datetimeEnd);
        }, function (err) {
            console.log('Error occurred while getting date: ', err);
        });
    };
    AcDatePage.prototype.onSubmit = function () {
        var start = this.dateDatetime.value.startDate;
        var end = this.dateDatetime.value.endDate;
        console.log("start : " + start);
        console.log("end : " + end);
        this.callAPI(start, end);
    };
    AcDatePage.prototype.callAPI = function (strat, end) {
        var _this = this;
        var idDevice = this.navParams.get("idDevice");
        var url = "http://167.71.222.155:8080/api/log/ac/" + idDevice + "/" + strat + "/" + end;
        console.log(url);
        this.graphData = this.httpClient.get(url);
        this.graphData
            .subscribe(function (data) {
            console.log(JSON.stringify(data));
            if (data['idStatus'] == 200) {
                var phase1 = data['phase1'];
                var phase2 = data['phase2'];
                var phase3 = data['phase3'];
                var date = data['dt'];
                console.log(phase1);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__graph_main_graph_main__["a" /* GraphMainPage */], { phase1: phase1, phase2: phase2, phase3: phase3, date: date });
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: data['nameStatus'],
                    duration: 3000,
                    position: 'top'
                });
                toast.present();
            }
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    AcDatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-ac-date',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/ac-date/ac-date.html"*/`<!--\n  Generated template for the SelectDataPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Select Date</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n  <form [formGroup]="dateDatetime" (ngSubmit)="onSubmit()">\n\n    <ion-item no-lines style="margin-bottom: 10px;">\n    <ion-label style="color: black">Start time</ion-label>\n\n    <ion-input disabled="false" type="text" ion-text style="color :black" \n      (click)="startTime()"  formControlName="startDate"></ion-input>\n  </ion-item>\n\n  <ion-item no-lines style="margin-bottom: 10px;">\n    <ion-label style="color: black">End time</ion-label>\n\n    <ion-input disabled="false" type="text" ion-text style="color :black" \n      (click)="endTime()"  formControlName="endDate"></ion-input>\n  </ion-item>\n\n  <button    color= "secondary" type="submit" [disabled]="!dateDatetime.valid" ion-button  full round>\n    ตกลง\n\n  </button>\n\n  </form>\n\n</ion-content>`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/ac-date/ac-date.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_date_picker__["a" /* DatePicker */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], AcDatePage);
    return AcDatePage;
}());

//# sourceMappingURL=ac-date.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GraphMainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the GraphMainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GraphMainPage = (function () {
    function GraphMainPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.colorBrown = "#a37207";
        this.colorBlack = "#030200";
        this.colorGray = "#706f6d";
        this.colorDC = "#e8651a";
        this.colorPoint = "#FFFFFF";
    }
    GraphMainPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GraphMainPage');
    };
    GraphMainPage.prototype.ngOnInit = function () {
        var phase1 = this.navParams.get("phase1");
        var phase2 = this.navParams.get("phase2");
        var phase3 = this.navParams.get("phase3");
        var date = this.navParams.get("date");
        this.lineChartPowerMethod(phase1, phase2, phase3, date);
    };
    GraphMainPage.prototype.lineChartPowerMethod = function (phase1, phase2, phase3, date) {
        var line = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.lineCanvasPower.nativeElement, {
            type: 'line',
            data: {
                labels: date,
                datasets: [
                    {
                        label: 'AC Phase 1',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: this.colorBrown,
                        borderColor: this.colorBrown,
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 7,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: this.colorBrown,
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: phase1,
                        spanGaps: false,
                    },
                    {
                        label: 'AC Phase 2',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: this.colorBlack,
                        borderColor: this.colorBlack,
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 7,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: this.colorBlack,
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: phase2,
                        spanGaps: false,
                    },
                    {
                        label: 'AC Phase 3',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: this.colorGray,
                        borderColor: this.colorGray,
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 7,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: this.colorGray,
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: phase3,
                        spanGaps: false,
                    }
                ]
            },
            options: {
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                    xAxes: [{
                            ticks: {
                                maxTicksLimit: 3,
                                autoSkip: true
                            },
                            display: true
                        }],
                }
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvasPower'),
        __metadata("design:type", Object)
    ], GraphMainPage.prototype, "lineCanvasPower", void 0);
    GraphMainPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-graph-main',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/graph-main/graph-main.html"*/`<!--\n  Generated template for the GraphMainPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header color="primary">\n\n  <ion-navbar color="primary">\n    <ion-title>Graph AC</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-header>\n      <!-- Power Chart -->\n    </ion-card-header>\n    <ion-card-content>\n      <canvas #lineCanvasPower></canvas>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/graph-main/graph-main.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], GraphMainPage);
    return GraphMainPage;
}());

//# sourceMappingURL=graph-main.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddDevicePage = (function () {
    function AddDevicePage(storage, toastCtrl, httpClient, navCtrl, navParams) {
        var _this = this;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.httpClient = httpClient;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.marker = null;
        this.stateMark = false;
        this.storage.get('idUser').then(function (val) {
            _this.idUser = val;
        });
        this.param = this.navParams.data;
        this.dataEdit = this.param['data'];
        console.log(JSON.stringify(this.dataEdit));
        this.title = this.param['title'];
        if (this.title == "Add Device") {
            this.btnText = "สร้าง";
        }
        else {
            this.btnText = "บันทึก";
        }
    }
    AddDevicePage.prototype.ngOnInit = function () {
        if (this.title == "Add Device") {
            this.addDevice = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormGroup */]({
                'nameDevice': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required),
                'placeDevice': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required),
                'latDevice': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required),
                'lngDevice': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required)
            });
        }
        else {
            this.addDevice = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormGroup */]({
                'nameDevice': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.dataEdit['name'], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required),
                'placeDevice': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.dataEdit['place'], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required),
                'latDevice': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.dataEdit['lat'], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required),
                'lngDevice': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.dataEdit['lng'], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required)
            });
        }
    };
    AddDevicePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        var latLng = new google.maps.LatLng(7.1545593, 100.6223298);
        var mapOptions = {
            center: latLng,
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        if (this.title == "Edit Device") {
            var latLng_1 = new google.maps.LatLng(this.dataEdit['lat'], this.dataEdit['lng']);
            this.marker = new google.maps.Marker({
                map: this.map,
                animation: google.maps.Animation.DROP,
                position: latLng_1,
                draggable: true
            });
        }
        google.maps.event.addListener(this.map, 'click', function (event) {
            _this.addMarker(event);
        });
    };
    AddDevicePage.prototype.addMarker = function (event) {
        if (this.stateMark == true || this.marker != null) {
            this.marker.setMap(null);
            this.stateMark = false;
        }
        this.marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: event.latLng,
            draggable: true
        });
        this.stateMark = true;
        var temp = JSON.stringify(event);
        var data = JSON.parse(temp);
        this.addDevice.patchValue({
            'latDevice': data['latLng']['lat'],
            'lngDevice': data['latLng']['lng']
        });
    };
    AddDevicePage.prototype.onSubmit = function () {
        var _this = this;
        if (this.title == "Add Device") {
            var nameDevice = this.addDevice.value.nameDevice;
            var placeDevice = this.addDevice.value.placeDevice;
            var latDevice = this.addDevice.value.latDevice;
            var lngDevice = this.addDevice.value.lngDevice;
            var body = {
                "name": nameDevice,
                "place": placeDevice,
                "lat": latDevice,
                "lng": lngDevice,
                "idUser": this.idUser
            };
            var httpHeaders = new __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json',
            });
            var options = {
                headers: httpHeaders
            };
            this.res = this.httpClient.post('http://167.71.222.155:8080/api/device', body, options);
            this.res
                .subscribe(function (data) {
                if (data['idStatus'] == 200) {
                    _this.addDevice.reset();
                    var toast = _this.toastCtrl.create({
                        message: 'เพิ่มอุปกรณ์สำเร็จ',
                        duration: 3000
                    });
                    toast.present();
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'เพิ่มอุปกรณ์ไม่สำเร็จ :' + data['nameStatus'],
                        duration: 3000
                    });
                    toast.present();
                }
            });
        }
        else {
            var nameDevice = this.addDevice.value.nameDevice;
            var placeDevice = this.addDevice.value.placeDevice;
            var latDevice = this.addDevice.value.latDevice;
            var lngDevice = this.addDevice.value.lngDevice;
            console.log(nameDevice);
            var body = {
                "name": nameDevice,
                "place": placeDevice,
                "lat": latDevice,
                "lng": lngDevice,
                "idDevice": this.dataEdit['idDevice']
            };
            var httpHeaders = new __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json',
            });
            var options = {
                headers: httpHeaders
            };
            this.res = this.httpClient.put('http://167.71.222.155:8080/api/device', body, options);
            this.res
                .subscribe(function (data) {
                if (data['idStatus'] == 200) {
                    var toast = _this.toastCtrl.create({
                        message: 'แก้ไขอุปกรณ์สำเร็จ',
                        duration: 3000
                    });
                    toast.present();
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'แก้ไขอุปกรณ์ไม่สำเร็จ :' + data['nameStatus'],
                        duration: 3000
                    });
                    toast.present();
                }
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AddDevicePage.prototype, "mapElement", void 0);
    AddDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-device',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/add-device/add-device.html"*/`<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>{{title}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  \n\n  <form [formGroup]="addDevice" (ngSubmit)="onSubmit()">\n\n\n\n    <ion-item no-lines style="margin-bottom: 10px;">\n      <ion-label floating>ชื่อ</ion-label>\n      <ion-input STYLE=\'color:#000000;\' type="text" formControlName="nameDevice"></ion-input>\n    </ion-item>\n    <ion-item no-lines style="margin-bottom: 10px;">\n      <ion-label floating>สถานที่</ion-label>\n      <ion-input STYLE=\'color:#000000;\' type="text" formControlName="placeDevice"></ion-input>\n    </ion-item>\n\n    <div #map id="map"></div>  \n    \n    <ion-item no-lines style="margin-bottom: 10px;">\n      <ion-label floating>ละติจูด</ion-label>\n      <ion-input STYLE=\'color:#000000;\' type="number" formControlName="latDevice"></ion-input>\n    </ion-item>\n    <ion-item no-lines style="margin-bottom: 10px;">\n      <ion-label floating>ลองจิจูด</ion-label>\n      <ion-input STYLE=\'color:#000000;\' type="number" formControlName="lngDevice"></ion-input>\n    </ion-item>\n\n      <button    color= "secondary" type="submit" [disabled]="!addDevice.valid" ion-button  full round>\n        {{btnText}}\n\n      </button>\n\n\n  </form>\n\n\n</ion-content>`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/add-device/add-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], AddDevicePage);
    return AddDevicePage;
}());

//# sourceMappingURL=add-device.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__log_device_log_device__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_device_add_device__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_launch_navigator__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sub_device_sub_device__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ac_date_ac_date__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the DevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DevicePage = (function () {
    function DevicePage(storage, launchNavigator, alertCtrl, navCtrl, navParams, httpClient) {
        var _this = this;
        this.storage = storage;
        this.launchNavigator = launchNavigator;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.item = [];
        this.page = __WEBPACK_IMPORTED_MODULE_4__log_device_log_device__["a" /* LogDevicePage */];
        this.storage.get('idUser').then(function (val) {
            _this.idUser = val;
            _this.devices = _this.httpClient.get('http://167.71.222.155:8080/api/device/' + _this.idUser);
            _this.devices
                .subscribe(function (data) {
                _this.item = (data['data']);
            });
        });
    }
    DevicePage.prototype.ngOnInit = function () {
        var _this = this;
        this.timeInterval = setInterval(function () {
            console.log("ewf");
            _this.storage.get('idUser').then(function (val) {
                _this.idUser = val;
                _this.devices = _this.httpClient.get('http://167.71.222.155:8080/api/device/' + _this.idUser);
                _this.devices
                    .subscribe(function (data) {
                    _this.item = (data['data']);
                });
            });
        }, 3000);
    };
    DevicePage.prototype.ionViewWillLeave = function () {
        clearInterval(this.timeInterval);
    };
    DevicePage.prototype.addDevice = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__add_device_add_device__["a" /* AddDevicePage */], { title: "Add Device" });
    };
    DevicePage.prototype.logDevice = function (index) {
        this.navCtrl.push(this.page, { data: this.item[index] });
    };
    DevicePage.prototype.graph = function (device) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__ac_date_ac_date__["a" /* AcDatePage */], { idDevice: device['idDevice'] });
    };
    DevicePage.prototype.editDevice = function (device) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__add_device_add_device__["a" /* AddDevicePage */], { title: "Edit Device", data: device });
    };
    DevicePage.prototype.deleteDevice = function (device) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'คำเตือน',
            message: 'คุณต้องการลบอุปกรณ์ ' + device['name'] + " ใช่หรือไม่",
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'OK',
                    handler: function () {
                        _this.okDelete(device);
                    }
                }
            ]
        });
        alert.present();
    };
    DevicePage.prototype.okDelete = function (device) {
        var _this = this;
        this.delete = this.httpClient.delete('http://167.71.222.155:8080/api/device/' + device['idDevice']);
        this.delete
            .subscribe(function (data) {
            console.log(JSON.stringify(data));
            if (data['idStatus'] == 200) {
                _this.devices = _this.httpClient.get('http://167.71.222.155:8080/api/device/' + _this.idUser);
                _this.devices
                    .subscribe(function (data) {
                    _this.item = (data['data']);
                });
            }
        });
    };
    DevicePage.prototype.gotoSub = function (idDevice) {
        console.log(idDevice);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__sub_device_sub_device__["a" /* SubDevicePage */], { device: idDevice });
    };
    DevicePage.prototype.gotoMap = function (device) {
        console.log(JSON.stringify(device));
        this.launchNavigator.navigate([7, 100]);
        ;
    };
    DevicePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.devices = this.httpClient.get('http://167.71.222.155:8080/api/device/' + this.idUser);
        this.devices
            .subscribe(function (data) {
            _this.item = (data['data']);
        });
    };
    DevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-device',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/device/device.html"*/`\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Device</ion-title>\n\n    <ion-buttons end>\n      <button ion-button icon-only (click)="addDevice()">\n        <ion-icon name="md-add"></ion-icon>\n      </button>\n    </ion-buttons>\n\n  </ion-navbar>\n\n  \n</ion-header>\n\n\n<ion-content padding>\n\n   \n\n  <ion-card   *ngFor="let device of item; let i = index" >\n    <ion-card-header>\n      \n      <ion-card-title>\n\n        <h2>{{device.name}}</h2>\n      </ion-card-title>\n    </ion-card-header>\n  \n    <ion-card-content>\n        <p>{{device.place}}</p>\n        <p>กำลังไฟฟ้าที่ผลิตได้ {{device.power}} W</p>\n        <br>\n        <button color="primary" ion-button outline (click)="gotoSub(device.idDevice)" >ข้อมูล</button>\n        <button color="secondary" ion-button outline (click)="graph(device)" >กราฟ</button>\n        <button color="danger" (click) =  "gotoMap(device)" ion-button outline  >นำทาง</button>\n\n        <ion-item>\n         \n          <button (click) ="editDevice(device)" ion-button color ="alert" icon-start clear item-end>\n            <ion-icon name="ios-create"></ion-icon>\n            แก้ไข\n          </button>\n\n          <button  (click) = "deleteDevice(device)" ion-button color ="danger" icon-start clear item-end>\n            <ion-icon name="ios-trash"></ion-icon>\n            ลบ\n          </button>\n\n        </ion-item>\n\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/device/device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_launch_navigator__["a" /* LaunchNavigator */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], DevicePage);
    return DevicePage;
}());

//# sourceMappingURL=device.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LogDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the LogDevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LogDevicePage = (function () {
    function LogDevicePage(navCtrl, navParams, httpClient) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        var idDevice = this.navParams.get('data')['idDevice'];
        this.logs = this.httpClient.get('https://128.199.107.134:8181/api/device/log/' + idDevice + "/1");
        this.logs
            .subscribe(function (data) {
            _this.item = data['data'];
            console.log(JSON.stringify(_this.item));
        });
    }
    LogDevicePage.prototype.ionViewDidLoad = function () {
        this.title = this.navParams.get('data')['name'];
    };
    LogDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-log-device',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/log-device/log-device.html"*/`<!--\n  Generated template for the LogDevicePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{title}}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card   *ngFor="let log of item; let i = index"   >\n    <ion-card-header>\n      \n      <ion-card-title>{{log.dt}}</ion-card-title>\n    </ion-card-header>\n  \n    <ion-card-content>\n        <p>แรงดัน {{log.vol}} V</p>\n        <p>กระแส {{log.amp}} A</p>\n        <p>กำลังไฟฟ้า {{log.amp * log.vol}} Watt</p>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/log-device/log-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], LogDevicePage);
    return LogDevicePage;
}());

//# sourceMappingURL=log-device.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectDataPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__graph_graph__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SelectDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SelectDataPage = (function () {
    function SelectDataPage(toastCtrl, httpClient, datePicker, navCtrl, navParams) {
        this.toastCtrl = toastCtrl;
        this.httpClient = httpClient;
        this.datePicker = datePicker;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.powerX = [];
        this.powerY = [];
    }
    SelectDataPage.prototype.ngOnInit = function () {
        this.dateDatetime = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* FormGroup */]({
            'startDate': new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["h" /* Validators */].required),
            'endDate': new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](null, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["h" /* Validators */].required)
        });
    };
    SelectDataPage.prototype.startTime = function () {
        var _this = this;
        this.datePicker.show({
            date: new Date(),
            mode: 'datetime',
            is24Hour: true,
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(function (date) {
            var m;
            if (date.getMonth() == 12) {
                m = 1;
            }
            else {
                m = date.getMonth() + 1;
            }
            _this.datetimeStart = date.getFullYear() + '-' + m + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
            _this.dateDatetime.patchValue({
                'startDate': _this.datetimeStart
            });
            console.log('Got date: ', _this.datetimeStart);
        }, function (err) {
            console.log('Error occurred while getting date: ', err);
        });
    };
    SelectDataPage.prototype.endTime = function () {
        var _this = this;
        this.datePicker.show({
            date: new Date(),
            mode: 'datetime',
            is24Hour: true,
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
        }).then(function (date) {
            var m;
            if (date.getMonth() == 12) {
                m = 1;
            }
            else {
                m = date.getMonth() + 1;
            }
            _this.datetimeEnd = date.getFullYear() + '-' + m + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
            _this.dateDatetime.patchValue({
                'endDate': _this.datetimeEnd
            });
            console.log('Got date: ', _this.datetimeEnd);
        }, function (err) {
            console.log('Error occurred while getting date: ', err);
        });
    };
    SelectDataPage.prototype.onSubmit = function () {
        var start = this.dateDatetime.value.startDate;
        var end = this.dateDatetime.value.endDate;
        console.log("start : " + start);
        console.log("end : " + end);
        this.callAPI(start, end);
    };
    SelectDataPage.prototype.callAPI = function (strat, end) {
        var _this = this;
        var id = this.navParams.get("idSub");
        var url = "http://167.71.222.155:8080/api/log/" + id + "/" + strat + "/" + end;
        console.log(url);
        this.graphData = this.httpClient.get(url);
        this.graphData
            .subscribe(function (data) {
            if (data['idStatus'] == 200) {
                var key = Object.keys(data['data']);
                var powerStr = JSON.stringify(data['data']);
                var power = JSON.parse(powerStr);
                for (var x = 0; x < key.length; x++) {
                    _this.powerX.push(power[x]['dt']);
                }
                for (var y = 0; y < key.length; y++) {
                    _this.powerY.push(power[y]['power']);
                }
                console.log(JSON.stringify(_this.powerX));
                console.log(JSON.stringify(_this.powerY));
                var id = _this.navParams.get("idSub");
                var phase = _this.navParams.get("phase");
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__graph_graph__["a" /* GraphPage */], { x: _this.powerX, y: _this.powerY, id: id, phase: phase });
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: data['nameStatus'],
                    duration: 3000,
                    position: 'top'
                });
                toast.present();
            }
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    SelectDataPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-select-data',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/select-data/select-data.html"*/`<!--\n  Generated template for the SelectDataPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Select Date</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n  <form [formGroup]="dateDatetime" (ngSubmit)="onSubmit()">\n\n    <ion-item no-lines style="margin-bottom: 10px;">\n    <ion-label style="color: black">Start time</ion-label>\n\n    <ion-input disabled="false" type="text" ion-text style="color :black" \n      (click)="startTime()"  formControlName="startDate"></ion-input>\n  </ion-item>\n\n  <ion-item no-lines style="margin-bottom: 10px;">\n    <ion-label style="color: black">End time</ion-label>\n\n    <ion-input disabled="false" type="text" ion-text style="color :black" \n      (click)="endTime()"  formControlName="endDate"></ion-input>\n  </ion-item>\n\n  <button    color= "secondary" type="submit" [disabled]="!dateDatetime.valid" ion-button  full round>\n    ตกลง\n\n  </button>\n\n  </form>\n\n</ion-content>`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/select-data/select-data.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_date_picker__["a" /* DatePicker */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], SelectDataPage);
    return SelectDataPage;
}());

//# sourceMappingURL=select-data.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GraphPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the GraphPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GraphPage = (function () {
    function GraphPage(httpClient, navCtrl, navParams) {
        this.httpClient = httpClient;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.powerX = [];
        this.powerY = [];
        this.colorBrown = "#a37207";
        this.colorBlack = "#030200";
        this.colorGray = "#706f6d";
        this.colorDC = "#e8651a";
        this.colorPoint = "#FFFFFF";
    }
    GraphPage.prototype.ionViewDidLoad = function () {
    };
    GraphPage.prototype.ngOnInit = function () {
        this.idSub = this.navParams.get("id");
        this.phase = this.navParams.get("phase");
        this.powerX = this.navParams.get("x");
        this.powerY = this.navParams.get("y");
        if (this.phase == 1) {
            this.colorLine = this.colorBrown;
        }
        else if (this.phase == 2) {
            this.colorLine = this.colorBlack;
        }
        else if (this.phase == 3) {
            this.colorLine = this.colorGray;
        }
        else {
            this.colorLine = this.colorDC;
        }
        this.lineChartPowerMethod();
        console.log(this.phase + " " + this.powerX + " " + this.powerY);
    };
    GraphPage.prototype.lineChartPowerMethod = function () {
        var line = new __WEBPACK_IMPORTED_MODULE_2_chart_js__["Chart"](this.lineCanvasPower.nativeElement, {
            type: 'line',
            data: {
                labels: this.powerX,
                datasets: [
                    {
                        label: 'กำลังไฟฟ้า',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: this.colorLine,
                        borderColor: this.colorLine,
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 7,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: this.colorLine,
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: this.powerY,
                        spanGaps: false,
                    }
                ]
            },
            options: {
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                    xAxes: [{
                            ticks: {
                                maxTicksLimit: 3,
                                autoSkip: true
                            },
                            display: true
                        }],
                }
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvasVol'),
        __metadata("design:type", Object)
    ], GraphPage.prototype, "lineCanvasVol", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvasAmp'),
        __metadata("design:type", Object)
    ], GraphPage.prototype, "lineCanvasAmp", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvasPower'),
        __metadata("design:type", Object)
    ], GraphPage.prototype, "lineCanvasPower", void 0);
    GraphPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-graph',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/graph/graph.html"*/`<!--\n  Generated template for the GraphPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Graph</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-card>\n        <ion-card-header>\n          <!-- Power Chart -->\n        </ion-card-header>\n        <ion-card-content>\n          <canvas #lineCanvasPower></canvas>\n        </ion-card-content>\n      </ion-card>\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/graph/graph.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], GraphPage);
    return GraphPage;
}());

//# sourceMappingURL=graph.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tabs_tabs__ = __webpack_require__(355);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = (function () {
    function LoginPage(storage, httpClient, toastCtrl, formBuilder, navCtrl, navParams) {
        this.storage = storage;
        this.httpClient = httpClient;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.form = this.formBuilder.group({
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required]
        });
    }
    LoginPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.loginForm = function () {
        var _this = this;
        var user = this.form.value.username;
        var pass = this.form.value.password;
        var body = {
            "username": user,
            "password": pass
        };
        var httpHeaders = new __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["c" /* HttpHeaders */]({
            'Content-Type': 'application/json',
        });
        var options = {
            headers: httpHeaders
        };
        var res = this.httpClient.post('http://167.71.222.155:8080/api/user/login', body, options);
        res
            .subscribe(function (data) {
            if (data['idStatus'] == 200) {
                _this.storage.set('idUser', data['id']);
                console.log(JSON.stringify(data));
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__tabs_tabs__["a" /* TabsPage */]);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: '  Username หรือ รหัสผ่านผิด',
                    duration: 3000
                });
                toast.present();
            }
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/login/login.html"*/`<ion-header>\n  <!-- <ion-navbar>\n    <ion-title>\n      Login\n    </ion-title>\n  </ion-navbar> -->\n</ion-header>\n\n<ion-content scroll="false" padding class="login-background">\n\n\n  <ion-row class="row-margin">\n    <ion-col col-3>\n    </ion-col>\n    <ion-col col-6>\n      <img src="./assets/imgs/prick-s.png" class="logo-size">\n    </ion-col>\n    <ion-col col-3>\n    </ion-col>\n  </ion-row>\n\n  <ion-row class="row-topic">\n    <ion-col></ion-col>\n    <ion-col col-12>\n      <div>\n        <p class="d-flex justify-content-center font-topic">ระบบติดตามการบริหารจัดการน้ำและพลังงาน</p>\n        <p class="d-flex justify-content-center font-topic text-margin">ประปาชุมชนเทศบาลตำบลปริก</p>\n      </div>\n    </ion-col>\n    <ion-col></ion-col>\n  </ion-row>\n\n\n  <form [formGroup]="form" (submit)="loginForm()" >\n\n    <ion-row class="row-from">\n      <ion-col col-2>\n        <img src="./assets/imgs/avatar.png" class="img-size">\n      </ion-col>\n      <ion-col>\n        <ion-item class="input-text">\n          <ion-input placeholder="Username" formControlName="username" class="custom-input"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-2>\n        <img src="./assets/imgs/padlock.png" class="img-size">\n      </ion-col>\n      <ion-col>\n        <ion-item class="input-text">\n          <ion-input type = "password" placeholder="Password" formControlName="password" class="custom-input"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n\n\n\n    <div class="button-option">\n      <button ion-button round full color="green-bootstrap">Login</button>\n    </div>\n\n  </form>\n\n  <ion-row>\n    <ion-col></ion-col>\n\n    <ion-col>\n      <a (click)="register()" class="a-option">Register</a>\n    </ion-col>\n\n    <ion-col></ion-col>\n  </ion-row>\n\n\n\n\n\n\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = (function () {
    function RegisterPage(httpClient, navCtrl, navParams, formBuilder, toastCtrl) {
        this.httpClient = httpClient;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.form = this.formBuilder.group({
            fullname: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            username: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            repassword: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required]
        });
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.createRegis = function () {
        var _this = this;
        var user = this.form.value.username;
        var pass = this.form.value.password;
        var fullname = this.form.value.fullname;
        var repassword = this.form.value.repassword;
        if (pass == repassword) {
            var body = {
                "fullName": fullname,
                "username": user,
                "password": pass
            };
            var httpHeaders = new __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json',
            });
            var options = {
                headers: httpHeaders
            };
            var res = this.httpClient.post('http://167.71.222.155:8080/api/user/', body, options);
            res
                .subscribe(function (data) {
                console.log(JSON.stringify(data));
                if (data['idStatus'] == 200) {
                    var toast = _this.toastCtrl.create({
                        message: 'สมัครสมาชิกสำเร็จ',
                        duration: 3000
                    });
                    toast.present();
                    _this.navCtrl.pop();
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: 'ผิดพลาด',
                        duration: 3000
                    });
                    toast.present();
                }
            });
        }
        else {
            var toast = this.toastCtrl.create({
                message: 'รหัสผ่านไม่ตรงกัน',
                duration: 3000
            });
            toast.present();
        }
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/register/register.html"*/`<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>register</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content scroll="false" padding class="login-background">\n\n  <form [formGroup]="form" (submit)=createRegis()>\n    <ion-row class="row-from">\n      <ion-col col-2>\n        <img src="./assets/imgs/name.png" class="img-size">\n      </ion-col>\n      <ion-col>\n        <ion-item class="input-text">\n          <ion-input placeholder="ชื่อ-สกุล" formControllName="fullname" class="custom-input"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-2>\n        <img src="./assets/imgs/avatar.png" class="img-size">\n      </ion-col>\n      <ion-col>\n        <ion-item class="input-text">\n          <ion-input placeholder="Username" formControlName="username" class="custom-input"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-2>\n        <img src="./assets/imgs/padlock.png" class="img-size">\n      </ion-col>\n      <ion-col>\n        <ion-item class="input-text">\n          <ion-input placeholder="Password" formControlName="password" class="custom-input"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-2>\n        <img src="./assets/imgs/relock.png" class="img-size">\n      </ion-col>\n      <ion-col>\n        <ion-item class="input-text">\n          <ion-input placeholder="Re-Password" formControlName="repassword" class="custom-input"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <div class="button-option">\n      <button ion-button round full color="green-bootstrap">บันทึก</button>\n    </div>\n  </form>\n\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 176:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 176;

/***/ }),

/***/ 220:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/ac-date/ac-date.module": [
		825,
		10
	],
	"../pages/add-device/add-device.module": [
		826,
		9
	],
	"../pages/device/device.module": [
		827,
		8
	],
	"../pages/graph-main/graph-main.module": [
		828,
		7
	],
	"../pages/graph/graph.module": [
		829,
		6
	],
	"../pages/log-device/log-device.module": [
		830,
		5
	],
	"../pages/login/login.module": [
		831,
		4
	],
	"../pages/map-device/map-device.module": [
		832,
		0
	],
	"../pages/register/register.module": [
		833,
		3
	],
	"../pages/select-data/select-data.module": [
		834,
		2
	],
	"../pages/sub-device/sub-device.module": [
		835,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 220;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__contact_contact__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__device_device__ = __webpack_require__(158);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__device_device__["a" /* DevicePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_1__contact_contact__["a" /* ContactPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/tabs/tabs.html"*/`<ion-tabs color="primary">\n  <ion-tab [root]="tab1Root" tabTitle="Map" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Device" tabIcon="information-circle"></ion-tab>\n  <!-- <ion-tab [root]="tab3Root" tabTitle="Contact" tabIcon="contacts"></ion-tab> -->\n</ion-tabs>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/contact/contact.html"*/`<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Contact\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n    <ion-item>\n      <ion-icon name="ionic" item-start></ion-icon>\n      @ionicframework\n    </ion-item>\n  </ion-list>\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_screen_orientation__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sub_device_sub_device__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = (function () {
    function HomePage(storage, screenOrientation, navCtrl, httpClient) {
        // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        var _this = this;
        this.storage = storage;
        this.screenOrientation = screenOrientation;
        this.navCtrl = navCtrl;
        this.httpClient = httpClient;
        this.item = [];
        this.stateMark = false;
        this.mapState = false;
        this.markerArray = [];
        this.storage.get('idUser').then(function (val) {
            _this.idUser = val;
        });
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.storage.get('idUser').then(function (val) {
            _this.devices = _this.httpClient.get('http://167.71.222.155:8080/api/device/' + val);
            _this.devices
                .subscribe(function (data) {
                _this.item = (data['data']);
                _this.loadMap(_this.item);
            });
        });
    };
    HomePage.prototype.loadMap = function (item) {
        console.log(JSON.stringify(item));
        var m = [];
        if (item != null) {
            for (var i = 0; i < item.length; i++) {
                var temp = {};
                var latlng = {};
                latlng['lat'] = item[i]['lat'];
                latlng['lng'] = item[i]['lng'];
                temp['latlng'] = latlng;
                temp['title'] = item[i]['name'];
                temp['idDevice'] = item[i]['idDevice'];
                m.push(temp);
            }
            if (this.mapState == false) {
                var latLng = new google.maps.LatLng(7.1545593, 100.6223298);
                var mapOptions = {
                    center: latLng,
                    zoom: 6,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
                this.mapState = true;
            }
            this.markerArray = [];
            var markers = [];
            for (var i = 0; i < m.length; i++) {
                console.log(m[i]['title']);
                markers[i] = new google.maps.Marker({
                    map: this.map,
                    title: m[i]['title'],
                    animation: google.maps.Animation.DROP,
                    position: m[i]['latlng']
                });
                this.markerArray.push(markers[i]);
                this.stateMark = true;
                google.maps.event.addListener(markers[i], 'click', (function (marker, i, navCtrl) {
                    return function () {
                        console.log(JSON.stringify(m[i]['latlng']));
                        console.log(m[i]['idDevice']);
                        navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__sub_device_sub_device__["a" /* SubDevicePage */], { device: m[i]['idDevice'] });
                    };
                })(markers[i], i, this.navCtrl));
            }
        }
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        console.log("onViewDidEnter home");
        if (this.stateMark == true || this.marker != null) {
            this.stateMark = false;
            for (var i = 0; i < this.markerArray.length; i++) {
                this.markerArray[i].setMap(null);
            }
        }
        this.storage.get('idUser').then(function (val) {
            _this.devices = _this.httpClient.get('http://167.71.222.155:8080/api/device/' + val);
            _this.devices
                .subscribe(function (data) {
                _this.item = (data['data']);
                _this.loadMap(_this.item);
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "mapElement", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/home/home.html"*/`<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-padding>\n  <div #map id="map"></div>  \n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 490:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(491);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(495);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(543);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation_ngx__ = __webpack_require__(544);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_about_about__ = __webpack_require__(824);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_graph_graph__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_log_device_log_device__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_device_device__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_add_device_add_device__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_native_storage_ngx__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_launch_navigator__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_sub_device_sub_device__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_storage__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_screen_orientation__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_graph_main_graph_main__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_date_picker__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_select_data_select_data__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_ac_date_ac_date__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_login_login__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_register_register__ = __webpack_require__(163);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_graph_graph__["a" /* GraphPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_log_device_log_device__["a" /* LogDevicePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_device_device__["a" /* DevicePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_add_device_add_device__["a" /* AddDevicePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_sub_device_sub_device__["a" /* SubDevicePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_graph_main_graph_main__["a" /* GraphMainPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_select_data_select_data__["a" /* SelectDataPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_ac_date_ac_date__["a" /* AcDatePage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_register_register__["a" /* RegisterPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/ac-date/ac-date.module#AcDatePageModule', name: 'AcDatePage', segment: 'ac-date', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/add-device/add-device.module#AddDevicePageModule', name: 'AddDevicePage', segment: 'add-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/device/device.module#DevicePageModule', name: 'DevicePage', segment: 'device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/graph-main/graph-main.module#GraphMainPageModule', name: 'GraphMainPage', segment: 'graph-main', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/graph/graph.module#GraphPageModule', name: 'GraphPage', segment: 'graph', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/log-device/log-device.module#LogDevicePageModule', name: 'LogDevicePage', segment: 'log-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/map-device/map-device.module#MapDevicePageModule', name: 'MapDevicePage', segment: 'map-device', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/select-data/select-data.module#SelectDataPageModule', name: 'SelectDataPage', segment: 'select-data', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sub-device/sub-device.module#SubDevicePageModule', name: 'SubDevicePage', segment: 'sub-device', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_19__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_graph_graph__["a" /* GraphPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_log_device_log_device__["a" /* LogDevicePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_device_device__["a" /* DevicePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_add_device_add_device__["a" /* AddDevicePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_sub_device_sub_device__["a" /* SubDevicePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_graph_main_graph_main__["a" /* GraphMainPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_select_data_select_data__["a" /* SelectDataPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_ac_date_ac_date__["a" /* AcDatePage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_register_register__["a" /* RegisterPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation_ngx__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_launch_navigator__["a" /* LaunchNavigator */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_date_picker__["a" /* DatePicker */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_native_storage_ngx__["a" /* NativeStorage */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 522:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 227,
	"./af.js": 227,
	"./ar": 228,
	"./ar-dz": 229,
	"./ar-dz.js": 229,
	"./ar-kw": 230,
	"./ar-kw.js": 230,
	"./ar-ly": 231,
	"./ar-ly.js": 231,
	"./ar-ma": 232,
	"./ar-ma.js": 232,
	"./ar-sa": 233,
	"./ar-sa.js": 233,
	"./ar-tn": 234,
	"./ar-tn.js": 234,
	"./ar.js": 228,
	"./az": 235,
	"./az.js": 235,
	"./be": 236,
	"./be.js": 236,
	"./bg": 237,
	"./bg.js": 237,
	"./bm": 238,
	"./bm.js": 238,
	"./bn": 239,
	"./bn.js": 239,
	"./bo": 240,
	"./bo.js": 240,
	"./br": 241,
	"./br.js": 241,
	"./bs": 242,
	"./bs.js": 242,
	"./ca": 243,
	"./ca.js": 243,
	"./cs": 244,
	"./cs.js": 244,
	"./cv": 245,
	"./cv.js": 245,
	"./cy": 246,
	"./cy.js": 246,
	"./da": 247,
	"./da.js": 247,
	"./de": 248,
	"./de-at": 249,
	"./de-at.js": 249,
	"./de-ch": 250,
	"./de-ch.js": 250,
	"./de.js": 248,
	"./dv": 251,
	"./dv.js": 251,
	"./el": 252,
	"./el.js": 252,
	"./en-SG": 253,
	"./en-SG.js": 253,
	"./en-au": 254,
	"./en-au.js": 254,
	"./en-ca": 255,
	"./en-ca.js": 255,
	"./en-gb": 256,
	"./en-gb.js": 256,
	"./en-ie": 257,
	"./en-ie.js": 257,
	"./en-il": 258,
	"./en-il.js": 258,
	"./en-nz": 259,
	"./en-nz.js": 259,
	"./eo": 260,
	"./eo.js": 260,
	"./es": 261,
	"./es-do": 262,
	"./es-do.js": 262,
	"./es-us": 263,
	"./es-us.js": 263,
	"./es.js": 261,
	"./et": 264,
	"./et.js": 264,
	"./eu": 265,
	"./eu.js": 265,
	"./fa": 266,
	"./fa.js": 266,
	"./fi": 267,
	"./fi.js": 267,
	"./fo": 268,
	"./fo.js": 268,
	"./fr": 269,
	"./fr-ca": 270,
	"./fr-ca.js": 270,
	"./fr-ch": 271,
	"./fr-ch.js": 271,
	"./fr.js": 269,
	"./fy": 272,
	"./fy.js": 272,
	"./ga": 273,
	"./ga.js": 273,
	"./gd": 274,
	"./gd.js": 274,
	"./gl": 275,
	"./gl.js": 275,
	"./gom-latn": 276,
	"./gom-latn.js": 276,
	"./gu": 277,
	"./gu.js": 277,
	"./he": 278,
	"./he.js": 278,
	"./hi": 279,
	"./hi.js": 279,
	"./hr": 280,
	"./hr.js": 280,
	"./hu": 281,
	"./hu.js": 281,
	"./hy-am": 282,
	"./hy-am.js": 282,
	"./id": 283,
	"./id.js": 283,
	"./is": 284,
	"./is.js": 284,
	"./it": 285,
	"./it-ch": 286,
	"./it-ch.js": 286,
	"./it.js": 285,
	"./ja": 287,
	"./ja.js": 287,
	"./jv": 288,
	"./jv.js": 288,
	"./ka": 289,
	"./ka.js": 289,
	"./kk": 290,
	"./kk.js": 290,
	"./km": 291,
	"./km.js": 291,
	"./kn": 292,
	"./kn.js": 292,
	"./ko": 293,
	"./ko.js": 293,
	"./ku": 294,
	"./ku.js": 294,
	"./ky": 295,
	"./ky.js": 295,
	"./lb": 296,
	"./lb.js": 296,
	"./lo": 297,
	"./lo.js": 297,
	"./lt": 298,
	"./lt.js": 298,
	"./lv": 299,
	"./lv.js": 299,
	"./me": 300,
	"./me.js": 300,
	"./mi": 301,
	"./mi.js": 301,
	"./mk": 302,
	"./mk.js": 302,
	"./ml": 303,
	"./ml.js": 303,
	"./mn": 304,
	"./mn.js": 304,
	"./mr": 305,
	"./mr.js": 305,
	"./ms": 306,
	"./ms-my": 307,
	"./ms-my.js": 307,
	"./ms.js": 306,
	"./mt": 308,
	"./mt.js": 308,
	"./my": 309,
	"./my.js": 309,
	"./nb": 310,
	"./nb.js": 310,
	"./ne": 311,
	"./ne.js": 311,
	"./nl": 312,
	"./nl-be": 313,
	"./nl-be.js": 313,
	"./nl.js": 312,
	"./nn": 314,
	"./nn.js": 314,
	"./pa-in": 315,
	"./pa-in.js": 315,
	"./pl": 316,
	"./pl.js": 316,
	"./pt": 317,
	"./pt-br": 318,
	"./pt-br.js": 318,
	"./pt.js": 317,
	"./ro": 319,
	"./ro.js": 319,
	"./ru": 320,
	"./ru.js": 320,
	"./sd": 321,
	"./sd.js": 321,
	"./se": 322,
	"./se.js": 322,
	"./si": 323,
	"./si.js": 323,
	"./sk": 324,
	"./sk.js": 324,
	"./sl": 325,
	"./sl.js": 325,
	"./sq": 326,
	"./sq.js": 326,
	"./sr": 327,
	"./sr-cyrl": 328,
	"./sr-cyrl.js": 328,
	"./sr.js": 327,
	"./ss": 329,
	"./ss.js": 329,
	"./sv": 330,
	"./sv.js": 330,
	"./sw": 331,
	"./sw.js": 331,
	"./ta": 332,
	"./ta.js": 332,
	"./te": 333,
	"./te.js": 333,
	"./tet": 334,
	"./tet.js": 334,
	"./tg": 335,
	"./tg.js": 335,
	"./th": 336,
	"./th.js": 336,
	"./tl-ph": 337,
	"./tl-ph.js": 337,
	"./tlh": 338,
	"./tlh.js": 338,
	"./tr": 339,
	"./tr.js": 339,
	"./tzl": 340,
	"./tzl.js": 340,
	"./tzm": 341,
	"./tzm-latn": 342,
	"./tzm-latn.js": 342,
	"./tzm.js": 341,
	"./ug-cn": 343,
	"./ug-cn.js": 343,
	"./uk": 344,
	"./uk.js": 344,
	"./ur": 345,
	"./ur.js": 345,
	"./uz": 346,
	"./uz-latn": 347,
	"./uz-latn.js": 347,
	"./uz.js": 346,
	"./vi": 348,
	"./vi.js": 348,
	"./x-pseudo": 349,
	"./x-pseudo.js": 349,
	"./yo": 350,
	"./yo.js": 350,
	"./zh-cn": 351,
	"./zh-cn.js": 351,
	"./zh-hk": 352,
	"./zh-hk.js": 352,
	"./zh-tw": 353,
	"./zh-tw.js": 353
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 522;

/***/ }),

/***/ 543:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage_ngx__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = (function () {
    function MyApp(storage, nativeStorage, platform, statusBar, splashScreen) {
        this.storage = storage;
        this.nativeStorage = nativeStorage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/app/app.html"*/`<ion-nav [root]="rootPage"></ion-nav>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage_ngx__["a" /* NativeStorage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 824:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/about/about.html"*/`<ion-header>\n  <ion-navbar>\n    <ion-title>\n      About\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__select_data_select_data__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SubDevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SubDevicePage = (function () {
    function SubDevicePage(toastCtrl, storage, navCtrl, navParams, httpClient) {
        var _this = this;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.device = this.navParams.get("device");
        this.storage.get('idUser').then(function (val) {
            _this.idUser = val;
        });
    }
    SubDevicePage.prototype.gotoGraph = function (device) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__select_data_select_data__["a" /* SelectDataPage */], { idSub: device['id'], phase: device['phase'] });
        console.log(JSON.stringify(device));
    };
    SubDevicePage.prototype.ngOnInit = function () {
        var _this = this;
        this.addData = new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["c" /* FormGroup */]({
            'acPhase1Vol': new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */](0.0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* Validators */].required),
            'acPhase2Vol': new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */](0.0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* Validators */].required),
            'acPhase3Vol': new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */](0.0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* Validators */].required),
            'dcVol': new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */](0.0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* Validators */].required),
            'acPhase1Amp': new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */](0.0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* Validators */].required),
            'acPhase2Amp': new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */](0.0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* Validators */].required),
            'acPhase3Amp': new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */](0.0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* Validators */].required),
            'dcAmp': new __WEBPACK_IMPORTED_MODULE_5__angular_forms__["b" /* FormControl */](0.0, __WEBPACK_IMPORTED_MODULE_5__angular_forms__["h" /* Validators */].required),
        });
        var url;
        this.storage.get('idUser').then(function (val) {
            url = "http://167.71.222.155:8080/api/sub-device/" + val + "/" + _this.device;
            _this.subDevice = _this.httpClient.get(url);
            _this.subDevice
                .subscribe(function (data) {
                _this.item = data['data'];
                console.log(JSON.stringify(_this.item));
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        });
        this.timeInterval = setInterval(function () {
            var url = "http://167.71.222.155:8080/api/sub-device/" + _this.idUser + "/" + _this.device;
            console.log(url);
            _this.subDevice = _this.httpClient.get(url);
            _this.subDevice
                .subscribe(function (data) {
                _this.item = data['data'];
                console.log(JSON.stringify(_this.item));
            }, function (error) {
                console.log(JSON.stringify(error));
            });
        }, 3000);
    };
    SubDevicePage.prototype.ionViewWillLeave = function () {
        clearInterval(this.timeInterval);
    };
    SubDevicePage.prototype.onSubmit = function () {
        var _this = this;
        var acPhase1Vol = this.addData.value.acPhase1Vol;
        var acPhase2Vol = this.addData.value.acPhase2Vol;
        var acPhase3Vol = this.addData.value.acPhase3Vol;
        var dcVol = this.addData.value.dcVol;
        var acPhase1Amp = this.addData.value.acPhase1Amp;
        var acPhase2Amp = this.addData.value.acPhase2Amp;
        var acPhase3Amp = this.addData.value.acPhase3Amp;
        var dcAmp = this.addData.value.dcAmp;
        var url = "http://167.71.222.155:8080/api/sub-device/id/" + this.device;
        console.log(url);
        this.subDevice = this.httpClient.get(url);
        this.subDevice
            .subscribe(function (data) {
            console.log(JSON.stringify(data));
            if (data['idStatus'] == 200) {
                var urlRealtime = "http://167.71.222.155:8080/api/sub-device";
                data = [
                    {
                        id: 28,
                        realV: acPhase1Vol,
                        realA: acPhase1Amp
                    },
                    {
                        id: 29,
                        realV: acPhase2Vol,
                        realA: acPhase2Amp
                    },
                    {
                        id: 30,
                        realV: acPhase3Vol,
                        realA: acPhase3Amp
                    },
                    {
                        id: 31,
                        realV: dcVol,
                        realA: dcAmp
                    }
                ];
                for (var i = 0; i < data.length; i++) {
                    console.log("wef");
                    var httpHeaders = new __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["c" /* HttpHeaders */]({
                        'Content-Type': 'application/json',
                    });
                    var options = {
                        headers: httpHeaders
                    };
                    var a = _this.httpClient.put("http://167.71.222.155:8080/api/sub-device", data[i], options);
                    a.subscribe(function (data) {
                    }, function (error) {
                        console.log(JSON.stringify(error));
                    });
                }
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'ผิดพลาด',
                    duration: 3000
                });
                toast.present();
            }
        }, function (error) {
            console.log(JSON.stringify(error));
        });
    };
    SubDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sub-device',template:/*ion-inline-start:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/sub-device/sub-device.html"*/`<!--\n  Generated template for the SubDevicePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Sub Device</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <form [formGroup]="addData" (ngSubmit)="onSubmit()">\n\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n              <ion-item no-lines style="margin-bottom: 10px;">\n                  <ion-label floating  >แรงดัน AC IN P1</ion-label>\n                  <ion-input STYLE=\'color:#000000;\' type="number" formControlName="acPhase1Vol"></ion-input>\n                </ion-item>\n          </ion-col>\n          <ion-col>\n              <ion-item no-lines style="margin-bottom: 10px;">\n                  <ion-label floating>กระแส AC IN P1</ion-label>\n                  <ion-input STYLE=\'color:#000000;\' type="number" formControlName="acPhase1Amp"></ion-input>\n                </ion-item>\n            </ion-col>\n\n        </ion-row>\n\n\n          <ion-row>\n            <ion-col>\n                <ion-item no-lines style="margin-bottom: 10px;">\n                    <ion-label floating  >แรงดัน AC IN P2</ion-label>\n                    <ion-input STYLE=\'color:#000000;\' type="number" formControlName="acPhase2Vol"></ion-input>\n                  </ion-item>\n            </ion-col>\n            <ion-col>\n                <ion-item no-lines style="margin-bottom: 10px;">\n                    <ion-label floating>กระแส AC IN P2</ion-label>\n                    <ion-input STYLE=\'color:#000000;\' type="number" formControlName="acPhase2Amp"></ion-input>\n                  </ion-item>\n              </ion-col>\n\n          </ion-row>\n\n\n\n            <ion-row>\n              <ion-col>\n                  <ion-item no-lines style="margin-bottom: 10px;">\n                      <ion-label floating  >แรงดัน AC IN P3</ion-label>\n                      <ion-input STYLE=\'color:#000000;\' type="number" formControlName="acPhase3Vol"></ion-input>\n                    </ion-item>\n              </ion-col>\n              <ion-col>\n                  <ion-item no-lines style="margin-bottom: 10px;">\n                      <ion-label floating>กระแส AC IN P3</ion-label>\n                      <ion-input STYLE=\'color:#000000;\' type="number" formControlName="acPhase3Amp"></ion-input>\n                    </ion-item>\n                </ion-col>\n  \n            </ion-row>\n\n              <ion-row>\n                  <ion-col>\n                      <ion-item no-lines style="margin-bottom: 10px;">\n                          <ion-label floating  >แรงดัน DC IN</ion-label>\n                          <ion-input STYLE=\'color:#000000;\' type="number" formControlName="dcVol"></ion-input>\n                        </ion-item>\n                  </ion-col>\n                  <ion-col>\n                      <ion-item no-lines style="margin-bottom: 10px;">\n                          <ion-label floating>กระแส DC IN</ion-label>\n                          <ion-input STYLE=\'color:#000000;\' type="number" formControlName="dcAmp"></ion-input>\n                        </ion-item>\n                    </ion-col>\n      \n                </ion-row>\n              </ion-grid>\n\n    \n          <button    color= "secondary" type="submit" [disabled]="!addData.valid" ion-button  full round>\n            ส่งข้อมูล\n    \n          </button>\n    \n    \n      </form>\n\n  <ion-card *ngFor="let device of item; let i = index" (click)="gotoGraph(device)">\n\n    <ion-item>\n      <!-- <ion-avatar item-start>\n        <img src="img/marty-avatar.png">\n      </ion-avatar> -->\n      <h2>{{device.name}} {{device.nameType }} : {{device.namePhase}}</h2>\n      <p>{{device.dt}}</p>\n    </ion-item>\n  \n   \n  \n    <ion-card-content>\n      <p>{{device.realV | number:\'.2\' }} V. {{device.realA | number:\'.2\' }} A. </p>\n        <p>กำลังไฟฟ้า {{device.realV * device.realA | number:\'.2\' }} W</p>\n      \n    </ion-card-content>\n  \n    <!-- <ion-row>\n      <ion-col>\n        <button ion-button icon-start clear small>\n          <ion-icon name="thumbs-up"></ion-icon>\n          <div>12 Likes</div>\n        </button>\n      </ion-col>\n      <ion-col>\n        <button ion-button icon-start clear small>\n          <ion-icon name="text"></ion-icon>\n          <div>4 Comments</div>\n        </button>\n      </ion-col>\n      <ion-col align-self-center text-center>\n        <ion-note>\n          11h ago\n        </ion-note>\n      </ion-col>\n    </ion-row> -->\n  \n  </ion-card>\n</ion-content>\n`/*ion-inline-end:"/Volumes/9rachata/power-solar-prik/app-solar/src/pages/sub-device/sub-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], SubDevicePage);
    return SubDevicePage;
}());

//# sourceMappingURL=sub-device.js.map

/***/ })

},[490]);
//# sourceMappingURL=main.js.map