package com.sometime.solar.model;

import java.util.List;

public class AcLog extends Status {

	
	private List<Double> phase1;
	private List<Double> phase2;
	private List<Double> phase3;
	
	private List<String> dt;

	public List<Double> getPhase1() {
		return phase1;
	}

	public void setPhase1(List<Double> phase1) {
		this.phase1 = phase1;
	}

	public List<Double> getPhase2() {
		return phase2;
	}

	public void setPhase2(List<Double> phase2) {
		this.phase2 = phase2;
	}

	public List<Double> getPhase3() {
		return phase3;
	}

	public void setPhase3(List<Double> phase3) {
		this.phase3 = phase3;
	}

	public List<String> getDt() {
		return dt;
	}

	public void setDt(List<String> dt) {
		this.dt = dt;
	}
	
	
	
	
}
