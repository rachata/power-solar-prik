package com.sometime.solar.model;

import java.util.List;

public class SubDeviceRes extends Status {

	private List<SubDevice> data;

	public List<SubDevice> getData() {
		return data;
	}

	public void setData(List<SubDevice> data) {
		this.data = data;
	}
	
	public SubDeviceRes() {}
}
