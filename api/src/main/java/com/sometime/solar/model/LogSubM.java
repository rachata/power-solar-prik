package com.sometime.solar.model;

public class LogSubM {

	private int id;
	private int idSub;
	private double vol;
	private double amp;
	private String dt;
	
	
	public LogSubM() {}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getIdSub() {
		return idSub;
	}


	public void setIdSub(int idSub) {
		this.idSub = idSub;
	}


	public double getVol() {
		return vol;
	}


	public void setVol(double vol) {
		this.vol = vol;
	}


	public double getAmp() {
		return amp;
	}


	public void setAmp(double amp) {
		this.amp = amp;
	}


	public String getDt() {
		return dt;
	}


	public void setDt(String dt) {
		this.dt = dt;
	}



	
	
	
}
