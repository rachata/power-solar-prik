package com.sometime.solar.model;

import java.util.List;

public class LogSubRes extends Status{

	private List<LogPower> data;

	
	public LogSubRes() {}
	
	public List<LogPower> getData() {
		return data;
	}

	public void setData(List<LogPower> data) {
		this.data = data;
	}
	
	
	
}
