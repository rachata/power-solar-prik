package com.sometime.solar.model;

import java.util.List;

public class DeviceRes extends Status {

	private List<Device> data;

	public List<Device> getData() {
		return data;
	}

	public void setData(List<Device> data) {
		this.data = data;
	}
	
	public DeviceRes() {}
	
	
}
