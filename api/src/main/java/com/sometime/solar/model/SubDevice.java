package com.sometime.solar.model;

public class SubDevice {

	private int id;
	private String name;
	private int device;
	private double realV;
	private double realA;
	private int status;
	private int type;
	private String nameType;
	private String nameDevice;
	private String dt;
	private  int phase;
	
	private String namePhase;
	
	
	
	
	
	
	
	
	
	
	public String getNamePhase() {
		return namePhase;
	}
	public void setNamePhase(String namePhase) {
		this.namePhase = namePhase;
	}
	public int getPhase() {
		return phase;
	}
	public void setPhase(int phase) {
		this.phase = phase;
	}
	public String getDt() {
		return dt;
	}
	public void setDt(String dt) {
		this.dt = dt;
	}
	public String getNameType() {
		return nameType;
	}
	public void setNameType(String nameType) {
		this.nameType = nameType;
	}
	public String getNameDevice() {
		return nameDevice;
	}
	public void setNameDevice(String nameDevice) {
		this.nameDevice = nameDevice;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDevice() {
		return device;
	}
	public void setDevice(int device) {
		this.device = device;
	}
	public double getRealV() {
		return realV;
	}
	public void setRealV(double realV) {
		this.realV = realV;
	}
	public double getRealA() {
		return realA;
	}
	public void setRealA(double realA) {
		this.realA = realA;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public SubDevice(){}
	
	
}



