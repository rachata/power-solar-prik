package com.sometime.solar.model;

import java.util.List;

public class LogSubReq {

	private List<LogSubM> data;

	
	public LogSubReq() {}
	
	
	public List<LogSubM> getData() {
		return data;
	}

	public void setData(List<LogSubM> data) {
		this.data = data;
	}
	
	
}
