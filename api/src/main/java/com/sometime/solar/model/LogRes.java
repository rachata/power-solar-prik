package com.sometime.solar.model;

import java.util.List;

public class LogRes  extends Status{

	private List<Log> data;

	public List<Log> getData() {
		return data;
	}

	public void setData(List<Log> data) {
		this.data = data;
	}
	
	public LogRes() {}
	
	
}
