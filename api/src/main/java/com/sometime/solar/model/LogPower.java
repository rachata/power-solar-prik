package com.sometime.solar.model;

public class LogPower {

	private double power;
	private String dt;
	
	public LogPower() {}
	
	
	public double getPower() {
		return power;
	}
	public void setPower(double power) {
		this.power = power;
	}
	public String getDt() {
		return dt;
	}
	public void setDt(String dt) {
		this.dt = dt;
	}
	
	
}
