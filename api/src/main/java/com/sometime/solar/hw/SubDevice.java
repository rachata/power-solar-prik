package com.sometime.solar.hw;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.crypto.Data;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sometime.solar.model.Status;
import com.sometime.solar.model.SubDeviceRes;
import com.sometime.solar.utility.Database;

import com.sun.org.apache.xml.internal.security.keys.content.RetrievalMethod;

@RestController
@RequestMapping("/sub-device")
public class SubDevice {

	@RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE , consumes = MediaType.APPLICATION_JSON_VALUE )
	@CrossOrigin
	public ResponseEntity<Status> createSubDevice(@RequestBody com.sometime.solar.model.SubDevice subDevice){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		Status status  = new  Status();
		try {
			
			con = Database.connectDatabase();
			String sql = "insert into sub_devices (name , device , real_v , real_a , type ,phase) value(? , ? , ? , ? , ? , ?)";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, subDevice.getName());
			preparedStatement.setInt(2, subDevice.getDevice());
			preparedStatement.setDouble(3, 0.0);
			preparedStatement.setDouble(4, 0.0);
			preparedStatement.setInt(5,subDevice.getType());
			preparedStatement.setInt(6 , subDevice.getPhase());
			
			boolean statusSQL = preparedStatement.execute();
			
			if(!statusSQL) {
				
				status.setIdStatus(200);
				status.setNameStatus("Success");
			}else {
				
				status.setIdStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setIdStatus(500);
			status.setNameStatus("Unsuccess : " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	} 


	@RequestMapping(value = "/{idUser}/{idSub}" ,  method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<SubDeviceRes> getSubDeviceWithUser(@PathVariable("idUser") int idUser , @PathVariable("idSub") int  idSub){
		
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		SubDeviceRes subDeviceRes = new SubDeviceRes();
		try {
			
			con = Database.connectDatabase();
			
			String sql = "select sub_devices.id , line_type.id as \"phase\" , line_type.line_name  , sub_devices.name as \"subname\" ,devices.name as \"devicename\" ,  sub_devices.device , real_v , real_a , dt , type , status , name_type  from sub_devices\n" + 
					"inner join devices on devices.id = sub_devices.device\n" + 
					"inner join type_sub_device on type_sub_device.id  = sub_devices.type\n" + 
					"inner join line_type on line_type.id  = sub_devices.phase\n" + 
					"where devices.user  = ? and sub_devices.device = ?\n" + 
					"order by type\n" + 
					"";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idUser);
			preparedStatement.setInt(2, idSub);
			resultSet = preparedStatement.executeQuery();
			
			int count  =Database.countResultSet(resultSet);
			
			
			if(count >= 1) {
				resultSet.beforeFirst();
				subDeviceRes.setIdStatus(200);
				subDeviceRes.setNameStatus("Success");
				
				List<com.sometime.solar.model.SubDevice> list = new ArrayList<com.sometime.solar.model.SubDevice>();
				
				while(resultSet.next()) {
					com.sometime.solar.model.SubDevice subDevice = new com.sometime.solar.model.SubDevice();
					subDevice.setId(resultSet.getInt("id"));
					subDevice.setName(resultSet.getString("subname"));
					subDevice.setNameDevice(resultSet.getString("devicename"));
					subDevice.setDevice(resultSet.getInt("device"));
					subDevice.setRealV(resultSet.getDouble("real_v"));;
					subDevice.setRealA(resultSet.getDouble("real_a"));
					subDevice.setDt(resultSet.getString("dt"));
					subDevice.setType(resultSet.getInt("type"));
					subDevice.setStatus(resultSet.getInt("status"));
					subDevice.setNameType(resultSet.getString("name_type"));
					subDevice.setPhase(resultSet.getInt("phase"));
					subDevice.setNamePhase(resultSet.getString("line_name"));
					
					list.add(subDevice);
				}
				subDeviceRes.setData(list);
			}else {
				subDeviceRes.setIdStatus(204);
				subDeviceRes.setNameStatus("Unsuccess :No Data ");
			}
			
		} catch (SQLException e) {
			subDeviceRes.setIdStatus(500);
			subDeviceRes.setNameStatus("Unsuccess : " + e.getMessage());
			
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(subDeviceRes);
		}
	}

	@RequestMapping(method = RequestMethod.PUT , consumes = MediaType.APPLICATION_JSON_VALUE , produces =MediaType.APPLICATION_JSON_VALUE )
	@CrossOrigin
	
	public ResponseEntity<Status> updateReal(@RequestBody com.sometime.solar.model.SubDevice subDevice){
		
		Connection con = null;
		PreparedStatement preparedStatement =null;
		
		
		Status status = new Status();
		try {
			con = Database.connectDatabase();
			String sql ="update sub_devices set real_v = ? , real_a = ? , dt = ? where id = ?";
			
			SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date(System.currentTimeMillis());
			
			String datetime = formatter.format(date);

			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setDouble(1, subDevice.getRealV());
			preparedStatement.setDouble(2, subDevice.getRealA());
			preparedStatement.setString(3, datetime);
			preparedStatement.setInt(4, subDevice.getId());
			
			boolean statusSQL = preparedStatement.execute();
			if(!statusSQL) {
				status.setIdStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setIdStatus(204);
				status.setNameStatus("Unsuccess");
			}
		} catch (SQLException e) {
			status.setIdStatus(500);
			status.setNameStatus("Unsuccess : " + e.getMessage());
			
		}finally {
			Database.closeConnection(con);
			
			return ResponseEntity.ok().body(status);
			
		}
	}
	
	
	
	@RequestMapping(value = "/id/{idDevice}" , method = RequestMethod.GET)
	@CrossOrigin
	public ResponseEntity<Map<String, Integer>> getIDSub(@PathVariable("idDevice") int idDevice){
		
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		
		Map<String, Integer> map = new HashMap<String, Integer>();
		try {
			
			con = Database.connectDatabase();
			String sql ="\n" + 
					"select sub_devices.id , line_name from sub_devices\n" + 
					"inner join line_type on line_type.id = phase\n" + 
					"where device = ?";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idDevice);
			
			ResultSet res  = preparedStatement.executeQuery();
			int  count = Database.countResultSet(res);
			
			if(count >= 1) {
				map.put("idStatus", 200);
				
				res.beforeFirst();
				
				while(res.next()) {
					
					map.put(res.getString("line_name"), res.getInt("id"));
				}
			}else {
				map.put("idStatus", 204);
	
			}
		} catch (Exception e) {
			map.put("idStatus", 500);
			
		}finally {
			
			Database.closeConnection(con);
			return ResponseEntity.ok().body(map);
		}
		

 	}


}
