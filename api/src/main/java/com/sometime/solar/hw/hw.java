package com.sometime.solar.hw;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sometime.solar.model.Device;
import com.sometime.solar.model.DeviceRes;
import com.sometime.solar.model.Log;
import com.sometime.solar.model.LogRes;
import com.sometime.solar.model.Status;
import com.sometime.solar.utility.Database;

@RestController
@RequestMapping("/device")
public class hw {

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)

	@CrossOrigin
	public ResponseEntity<?> createDevice(@RequestBody Device device) {

		Status status = new Status();
		Connection con = null;
		PreparedStatement preparedStatement = null;

		try {
			con = Database.connectDatabase();

			String sql = "insert into devices(name , lat , lng , place , user) value (? , ? , ? , ? , ?);";
			preparedStatement = con.prepareStatement(sql);

			preparedStatement.setString(1, device.getName());
			preparedStatement.setDouble(2, device.getLat());
			preparedStatement.setDouble(3, device.getLng());
			preparedStatement.setString(4, device.getPlace());
			preparedStatement.setInt(5, device.getIdUser());

			boolean statusQuery = preparedStatement.execute();

			if (!statusQuery) {
				status.setIdStatus(200);
				status.setNameStatus("Success");
			} else {
				status.setIdStatus(204);
				status.setNameStatus("Unsuccess");
			}

		} catch (Exception e) {
			status.setIdStatus(200);
			status.setNameStatus("Error : " + e.getMessage());
		} finally {
			Database.closeConnection(con);

		}

		return ResponseEntity.ok().body(status);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)

	@CrossOrigin
	public ResponseEntity<?> updateDevice(@RequestBody Device device) {

		Status status = new Status();
		Connection con = null;
		PreparedStatement preparedStatement = null;

		try {
			con = Database.connectDatabase();

			String sql = "update  devices set name = ?  , lat = ?  ,  lng = ? ,   place = ? where id = ?";
			preparedStatement = con.prepareStatement(sql);

			preparedStatement.setString(1, device.getName());
			preparedStatement.setDouble(2, device.getLat());
			preparedStatement.setDouble(3, device.getLng());
			preparedStatement.setString(4, device.getPlace());
			preparedStatement.setInt(5, device.getIdDevice());
			
			boolean statusQuery = preparedStatement.execute();

			if (!statusQuery) {
				status.setIdStatus(200);
				status.setNameStatus("Success");
			} else {
				status.setIdStatus(204);
				status.setNameStatus("Unsuccess");
			}

		} catch (Exception e) {
			status.setIdStatus(200);
			status.setNameStatus("Error : " + e.getMessage());
		} finally {
			Database.closeConnection(con);

		}

		return ResponseEntity.ok().body(status);
	}

	
	
	@RequestMapping( value = "/{id}" , method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)

	@CrossOrigin
	public ResponseEntity<?> deleteDevice(@PathVariable("id") int id) {

		Status status = new Status();
		Connection con = null;
		PreparedStatement preparedStatement = null;

		try {
			con = Database.connectDatabase();

			String sql = "delete from devices where id = ?";
			preparedStatement = con.prepareStatement(sql);

			preparedStatement.setInt(1, id);

			
			boolean statusQuery = preparedStatement.execute();

			if (!statusQuery) {
				status.setIdStatus(200);
				status.setNameStatus("Success");
			} else {
				status.setIdStatus(204);
				status.setNameStatus("Unsuccess");
			}

		} catch (Exception e) {
			status.setIdStatus(200);
			status.setNameStatus("Error : " + e.getMessage());
		} finally {
			Database.closeConnection(con);

		}

		return ResponseEntity.ok().body(status);
	}
	
	
	

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<DeviceRes> getDevice() {

		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet res = null;

		DeviceRes deviceRes = new DeviceRes();
		try {
			con = Database.connectDatabase();

			String sql = "select * from devices;";
			preparedStatement = con.prepareStatement(sql);

			res = preparedStatement.executeQuery();

			int count = Database.countResultSet(res);
			if (count >= 1) {

				deviceRes.setIdStatus(200);
				deviceRes.setNameStatus("Success");
				res.beforeFirst();

				List<Device> list = new ArrayList();
				while (res.next()) {
					Device device = new Device();
					device.setIdDevice(res.getInt("id"));
					device.setIdUser(res.getInt("user"));
					device.setName(res.getString("name"));
					device.setLat(res.getDouble("lat"));
					device.setLng(res.getDouble("lng"));
					device.setPlace(res.getString("place"));

					
					String sqlReal = "select SUM(p) as total_p from \n" + 
							"(SELECT  (real_v * real_a) as p  FROM sub_devices  where (type = 1 or type = 2) and device = ?) as da";
					preparedStatement = con.prepareStatement(sqlReal);
					preparedStatement.setInt(1,res.getInt("id") );
					
					ResultSet re = preparedStatement.executeQuery();
					
					System.out.println(Database.countResultSet(re));
					
					if(Database.countResultSet(re) == 1) {
						re.first();
						device.setPower(re.getDouble("total_p"));
					}else {
						device.setPower(0.0);
						
					}
					list.add(device);
				}
				deviceRes.setData(list);
			} else {

				deviceRes.setIdStatus(204);
				deviceRes.setNameStatus("No Data");

			}

		} catch (SQLException e) {
			deviceRes.setIdStatus(500);
			deviceRes.setNameStatus("Error : " + e.getErrorCode() + " " + e.getMessage());
		} finally {
			Database.closeConnection(con);

		}

		return ResponseEntity.ok().body(deviceRes);

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<DeviceRes> getDeviceOfUser(@PathVariable("id") int id) {

		Connection con = null;
		PreparedStatement preparedStatement = null;
		ResultSet res = null;

		DeviceRes deviceRes = new DeviceRes();
		try {
			con = Database.connectDatabase();

			String sql = "select * from devices where user = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, id);

			res = preparedStatement.executeQuery();

			int count = Database.countResultSet(res);
			if (count >= 1) {

				deviceRes.setIdStatus(200);
				deviceRes.setNameStatus("Success");
				res.beforeFirst();

				List<Device> list = new ArrayList();
				while (res.next()) {
					Device device = new Device();
					device.setIdDevice(res.getInt("id"));
					device.setIdUser(res.getInt("user"));
					device.setName(res.getString("name"));
					device.setLat(res.getDouble("lat"));
					device.setLng(res.getDouble("lng"));
					device.setPlace(res.getString("place"));

//					String sqlReal = "select SUM(p) as total_p from \n" + 
//							"(SELECT  (real_v * real_a) as p  FROM sub_devices  where (type = 1 or type = 2) and device = ?) as da";
//					
					
					
					String sqlReal = "select SUM(p) as total_p from \n" + 
							"(SELECT  (real_v * real_a) as p  FROM sub_devices  where type = 1  and device = ?) as da";
					
					preparedStatement = con.prepareStatement(sqlReal);
					preparedStatement.setInt(1,res.getInt("id") );
					
					ResultSet re = preparedStatement.executeQuery();
					
					
					
					if(Database.countResultSet(re) == 1) {
						re.first();
						device.setPower(re.getDouble("total_p"));
					}else {
						device.setPower(0.0);
						
					}
					
					list.add(device);
				}
				deviceRes.setData(list);
			} else {

				deviceRes.setIdStatus(204);
				deviceRes.setNameStatus("No Data");

			}

		} catch (SQLException e) {
			deviceRes.setIdStatus(500);
			deviceRes.setNameStatus("Error : " + e.getErrorCode() + " " + e.getMessage());
		} finally {
			Database.closeConnection(con);

		}

		return ResponseEntity.ok().body(deviceRes);

	}

	
}
