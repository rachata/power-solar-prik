package com.sometime.solar.hw;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import javax.print.attribute.standard.Media;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sometime.solar.model.AcLog;
import com.sometime.solar.model.LogPower;
import com.sometime.solar.model.LogSubM;
import com.sometime.solar.model.LogSubReq;
import com.sometime.solar.model.LogSubRes;
import com.sometime.solar.model.Status;
import com.sometime.solar.utility.Database;

@RestController
@RequestMapping("/log")
public class LogSub  {

	@RequestMapping(method = RequestMethod.POST , produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<Status> createLog(@RequestBody LogSubReq logSubReq){
		
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		Status status = new Status();
		try {
			
			System.out.println(logSubReq.getData().size());
			
			con = Database.connectDatabase();
			String sql = "insert into log_sub (id_sub , v , a , dt) value(? , ? ,? , ?)";
			
			preparedStatement = con.prepareStatement(sql);
			
	
			
//			SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			Date date = new Date(System.currentTimeMillis());
//			
//			
			
			Date date = new Date(System.currentTimeMillis());
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			df.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));

			
			String datetime = df.format(date);
			
			for(LogSubM logSubM : logSubReq.getData()) {
				
				preparedStatement.setInt(1, logSubM.getIdSub());
				preparedStatement.setDouble(2, logSubM.getVol());
				preparedStatement.setDouble(3, logSubM.getAmp());
				preparedStatement.setString(4, datetime);
				preparedStatement.execute();
				
			}

			status.setIdStatus(200);
			status.setNameStatus("Success");

			
		} catch (SQLException e) {
			status.setIdStatus(500);
			status.setNameStatus("Unsuccess : " + e.getMessage());
			
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(status);
		}
	}

	
	
	@RequestMapping(value = "/{idSub}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<LogSubRes> getLogByID(@PathVariable("idSub") int idSub){
		
		
		Connection con = null;
		ResultSet resultSet = null;
		
		PreparedStatement preparedStatement = null;
		
		LogSubRes logSubRes = new LogSubRes();
		try {
			
			con = Database.connectDatabase();
			
			String sql ="select * from log_sub where id_sub = ?";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idSub);
			
			resultSet = preparedStatement.executeQuery();
			int count = Database.countResultSet(resultSet);
			if(count >= 1) {
				
				resultSet.beforeFirst();
				
				List<LogPower> list = new ArrayList<LogPower>();
				while(resultSet.next()) {
					double v = resultSet.getDouble("v");
					double a = resultSet.getDouble("a");
					
					double power = v * a;
					
					LogPower logPower = new LogPower();
					logPower.setDt(resultSet.getString("dt"));
					logPower.setPower(power);
					list.add(logPower);
				}
				
				logSubRes.setIdStatus(200);
				logSubRes.setNameStatus("Success");
				logSubRes.setData(list);
				
			}else {
				logSubRes.setIdStatus(204);
				logSubRes.setNameStatus("Unsuccess : No Data ");
			}
		} catch (SQLException e) {
			logSubRes.setIdStatus(500);
			logSubRes.setNameStatus("Unsuccess : " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(logSubRes);
		}
		
	}


	@RequestMapping(value = "/{idSub}/{dateStart}/{dateEnd}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<?> getLogDate(@PathVariable("idSub") int idSub , @PathVariable("dateStart") String dateStart , @PathVariable("dateEnd") String dateEnd){
		
		Connection con = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		LogSubRes logSubRes = new LogSubRes();
		try {
			
			con = Database.connectDatabase();
			String sql = "select * from log_sub \n" + 
					"where id_sub = ? and (dt BETWEEN ? AND ?)";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idSub);
			preparedStatement.setString(2, dateStart);
			preparedStatement.setString(3, dateEnd);
			
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			if(count >= 1) {
				resultSet.beforeFirst();
				List<LogPower> list = new ArrayList<LogPower>();
				
				while(resultSet.next()) {
					double v = resultSet.getDouble("v");
					double a = resultSet.getDouble("a");
					
					double power = v * a;
					
					LogPower logPower = new LogPower();
					logPower.setDt(resultSet.getString("dt"));
					logPower.setPower(power);
					list.add(logPower);
				}
				
				logSubRes.setIdStatus(200);
				logSubRes.setNameStatus("Success");
				logSubRes.setData(list);
			}else {
				logSubRes.setIdStatus(204);
				logSubRes.setNameStatus("Unsuccess :No Data");
			}
		} catch (SQLException e) {
			logSubRes.setIdStatus(500);
			logSubRes.setNameStatus("Unsuccess : " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(logSubRes);
		}
		
	}


	
	@RequestMapping(value = "/ac/{idDevice}/{dateStart}/{dateEnd}" , method = RequestMethod.GET , produces = MediaType.APPLICATION_JSON_VALUE)
	@CrossOrigin
	public ResponseEntity<AcLog> getLogDateAC(@PathVariable("idDevice") int idSub , @PathVariable("dateStart") String dateStart , @PathVariable("dateEnd") String dateEnd){
		
		Connection con = null;
		ResultSet resultSet = null;
		PreparedStatement preparedStatement = null;
		AcLog acLog = new AcLog();
		try {
			
			con = Database.connectDatabase();
			String sql = "select log_sub.id_sub , log_sub.v , log_sub.a , log_sub.dt , sub_devices.phase  from log_sub\n" + 
					"inner join sub_devices on sub_devices.id = id_sub\n" + 
					"where sub_devices.device = ? and sub_devices.phase >=1 and sub_devices.phase <= 3\n" + 
					"and sub_devices.type = 1 and (log_sub.dt BETWEEN ? AND ?)\n" + 
					"order by log_sub.id ASC, log_sub.dt ASC";
			
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setInt(1, idSub);
			preparedStatement.setString(2, dateStart);
			preparedStatement.setString(3, dateEnd);
			
			resultSet = preparedStatement.executeQuery();
			
			int count = Database.countResultSet(resultSet);
			
			if(count >= 1) {
				resultSet.beforeFirst();
				
				List<Double> phase1 = new ArrayList<Double>();
				List<Double> phase2 = new ArrayList<Double>();
				List<Double> phase3 = new ArrayList<Double>();
				List<String> dt = new ArrayList<String>();
				
				while(resultSet.next()) {
					
					if(!dt.contains(resultSet.getString("dt"))) {
						dt.add(resultSet.getString("dt"));
					}
					 double v = resultSet.getDouble("v");
					 double a = resultSet.getDouble("a");
					
					 double p = v * a;
					 if((int) resultSet.getInt("phase") == 1) {
						 phase1.add(p);
					 }else if((int) resultSet.getInt("phase") == 2) {
						 phase2.add(p);
					 }else if((int) resultSet.getInt("phase") == 3) {
						 phase3.add(p);
					 }
					 
					 
				}
				
				acLog.setIdStatus(200);
				acLog.setNameStatus("Success");
				acLog.setPhase1(phase1);
				acLog.setPhase2(phase2);
				acLog.setPhase3(phase3);
				acLog.setDt(dt);
				
			}else {
				acLog.setIdStatus(500);
				acLog.setNameStatus("Unsuccess : No Data");
			}
		} catch (SQLException e) {
			acLog.setIdStatus(500);
			acLog.setNameStatus("Unsuccess : " + e.getMessage());
		}finally {
			Database.closeConnection(con);
			return ResponseEntity.ok().body(acLog);
		}
		
	}

	


	
}
