package com.sometime.solar.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {
	static final private String username = "solar";
	static final private String password = "@#Solar2019";
	static final private String host = "167.71.222.155";
	static final private String database = "solar";
	
	
	public static Connection connectDatabase() {
		Connection connect = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://"+host+"/"+database+"?useUnicode=true&characterEncoding=utf-8&user="+username+"&password="+password);

			if (connect != null) {
				return connect;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	public static boolean closeConnection(Connection con) {
		try {
			con.close();
			return true;
		} catch (SQLException e) {
			return false;
		}
		
	}
	
	
	public static int countResultSet(ResultSet rs) {
		
		int size =0;
		if (rs != null) {
		  try {
			rs.last();
			size = rs.getRow(); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    
		  
		 
		}
		 return size;
	}
	
}
