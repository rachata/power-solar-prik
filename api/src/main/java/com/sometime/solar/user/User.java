package com.sometime.solar.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sometime.solar.model.Status;
import com.sometime.solar.utility.Database;

@RestController
@RequestMapping("/user")
public class User {
	
	@RequestMapping(method = RequestMethod.POST , 
					produces = MediaType.APPLICATION_JSON_VALUE,
					consumes = MediaType.APPLICATION_JSON_VALUE)
	
	@CrossOrigin
	public ResponseEntity<Status> createUser(@RequestBody com.sometime.solar.model.User user){
		
		
		
		Status status = new Status();
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		
		try {
			con = Database.connectDatabase();
			
			String sql = "insert into s_user(username , password , fullname) value (? , ? , ?)";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getPassword());
			preparedStatement.setString(3, user.getFullName());
			
			boolean statusQuery = preparedStatement.execute();
			
			
			if(!statusQuery) {
				status.setIdStatus(200);
				status.setNameStatus("Success");
			}else {
				status.setIdStatus(204);
				status.setNameStatus("Unsuccess");
			}
			
		} catch (Exception e) {
			status.setIdStatus(200);
			status.setNameStatus("Error : " + e.getMessage());
		}finally {
			Database.closeConnection(con);
		}
		
		return ResponseEntity.ok().body(status);
	}

	
	
	
	@RequestMapping(value = "/login",
					method = RequestMethod.POST , 
					produces = MediaType.APPLICATION_JSON_VALUE,
					consumes = MediaType.APPLICATION_JSON_VALUE)
	
	@CrossOrigin
	
	public ResponseEntity<com.sometime.solar.model.User> login(@RequestBody com.sometime.solar.model.User user){
		
		
	
		com.sometime.solar.model.User userResult = new  com.sometime.solar.model.User();
		Connection con = null;
		PreparedStatement preparedStatement = null;
		
		
		try {
			con = Database.connectDatabase();
			
			String sql = "select * from s_user where username = ? and password = ?";
			preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, user.getUsername());
			preparedStatement.setString(2, user.getPassword());
			
			
			ResultSet res = preparedStatement.executeQuery();
			res.beforeFirst();
			int count = Database.countResultSet(res);
			if(count == 1) {
				userResult.setIdStatus(200);
				userResult.setNameStatus("Success");
				userResult.setId(res.getInt("id"));
				userResult.setFullName(res.getString("fullname"));
			}else {
				userResult.setIdStatus(204);
				userResult.setNameStatus("Unsuccess : No User");
			}
			
		} catch (Exception e) {
			userResult.setIdStatus(500);
			userResult.setNameStatus("Error : " + e.getMessage());
		}finally {
			Database.closeConnection(con);
		}
		
		return ResponseEntity.ok().body(userResult);
		
	} 
}
